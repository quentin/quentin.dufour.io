---
title: About
permalink: about/
profile: true
---

Bonjour chèr·e internaute,

J'ai mis en ligne ce site en 2012, encore lycéen, pour partager mes découvertes en informatique.
Depuis, l'informatique est devenu mon métier.
La suite de cette page devrait vous donner un aperçu de ce qui m'occupe au delà des billets de mon blog.

## Maintenant Maintenant Maintenant

J'améliore le code et l'infrastructure de [Deuxfleurs](https://deuxfleurs.fr), notre hébergeur associatif expérimental où 
nous réfléchissons à comment rendre le numérique convivial, juste et émancipateur.
En parallèle, je cherche un emploi dans l'informatique qui soit en ligne avec l'éthique développée au sein de Deuxfleurs.

Lien : [Pourquoi cette section ?](https://nownownow.com/about)

## Contributions au logiciel libre

Au sein de [Deuxfleurs](https://deuxfleurs.fr),
nous développons notre propre suite de logiciel distribués : 
[Garage](https://git.deuxfleurs.fr/Deuxfleurs/garage/) pour le stockage de données, 
[Diplonat](https://git.deuxfleurs.fr/Deuxfleurs/diplonat) pour la configuration déclarative du réseau, 
[Bottin](https://git.deuxfleurs.fr/Deuxfleurs/bottin) pour la gestion des comptes,
et bien d'[autres logiciels](https://git.deuxfleurs.fr).
De manière individuelle, je maintiens un paquet communautaire pour [Chez Scheme](https://copr.fedorainfracloud.org/coprs/superboum/chez-scheme/).
J'ai également quelques contributions occasionnelles dans plusieurs logiciels libres, comme [Jenkins](https://github.com/jenkinsci/git-client-plugin/pull/214), [LXC](https://github.com/lxc/lxc/commit/f4152036dd29d59c99e6a9415d6ea121f69c88ec), [Matrix](https://github.com/matrix-org/synapse/pull/5138) ou [Plume](https://git.joinplu.me/Plume/Plume/pulls/825).

Liens : [Gitea](https://git.deuxfleurs.fr/quentin) --- [Github](https://github.com/superboum)

## Expérience professionnelle

Je suis diplomé du département informatique de l'[INSA Rennes](http://www.insa-rennes.fr).
Durant mes études, j'ai été en stage chez [OpenStudio](http://www.openstudio.fr), [Digitevent](http://www.digitevent.net), [Créancio](http://www.creancio.com), [Orckestra](https://www.orckestra.com) et l'[ANSSI](https://ssi.gouv.fr).
J'ai continué en thèse dans l'équipe [WIDE](https://team.inria.fr/wide/) à [Inria Rennes](https://www.inria.fr/centre/rennes).
J'ai travaillé sur les réseaux d'anonymat (Tor) et les protocoles gossip. J'ai publié les documents suivants :

  - Yérom-David Bromberg, Quentin Dufour, Davide Frey. Multisource Rumor Spreading with Network Coding. INFOCOM 2019 - IEEE International Conference on Computer Communications, Apr 2019, Paris, France. pp.1-10. [⟨hal-01946632⟩](https://hal.inria.fr/hal-01946632)
  - Quentin Dufour, sous la direction de Yérom-David Bromberg. High-throughput real-time onion networks to protect everyone’s privacy. [⟨theses-s204646⟩](http://theses.fr/s204646) 


Liens : [Thèse](/these.pdf) --- [CV](/cv.pdf) --- [LinkedIn](https://www.linkedin.com/in/qdufour/)


## Me contacter

Par email : `quentin``@``dufour.io`.  
Sur Matrix : `@``quentin``:``deuxfleurs.fr`

Liens : [Clé SSH](/assets/misc/quentin_ssh.txt) --- [Clé PGP](/assets/misc/quentin_pgp.txt)

## Sites amis

  * [Alex Auvolat](https://adnab.me/) - Computer Science, Systèmes Distribués
  * [Coline Aubert](https://colineaubert.com/) - Didactique visuelle, design graphique
  * [Esther Bouquet](https://estherbouquet.com) - Design graphique et d'espace
  * [Loïck Bonniot](https://blog.lesterpig.com/) - {Dev, Hack, Science, Privacy, OSS}
  * [Tristan Claverie](https://blog.tclaverie.eu/) - Sécurité
  * [Erwan Dufour](https://erwan.dufour.io/) - Système embarqué, électronique, impression 3D, découpeuse laser, maker
  * [Louison Gitzinger](https://louisongitzinger.com/) - Musique & Machine Learning, Android
  * [Jean Guégant](https://jguegant.github.io) - C++, security, game dev
  * [Florian Le Minoux](https://darkgallium.github.io/) - Système, Linux
  * [Adrien Luxey](https://luxeylab.net/) - Research, Decentralized Networks
  * [Maximilien Richer](http://mricher.fr/) - Système, DevOps

{% include footer.html %}
