---
layout: post
title: Votre site web autour d'un café
date: 2023-10-28T00:00:00.000+02:00
sitemap: false
---

<!--*Cette page est à destination des personnes qui ont un projet de site web
amateur que je prévois d'accompagner bénévolement en vue de les héberger chez [Deuxfleurs](https://deuxfleurs.fr). Bonne lecture.*-->

Vous voulez mettre un site web en ligne pour votre projet et vous cherchez *un geek* qui sache faire ? Halte là ! Je peux vous aider, mais ça va être un brin plus complexe. 

Un site web n'est pas qu'un défi technique : c'est avant tout du contenu, souvent textuel, parfois multimédia. Il y a donc un travail intellectuel préliminaire : il faut réfléchir à sa cible, ce qu'on veut lui transmettre, et prendre sa plus belle plume...

Le document qui suit vise à vous accompagner dans cette tâche. Une fois votre contenu prêt, nous pouvons nous retrouver un après-midi autour d'un café pour la mise en ligne.

## Avant propos

![Ville de Vesoul, image d'illustration](/assets/images/pages/vesoul.jpg)

Avant de nous lancer, se pose la question de savoir si je suis le bon interlocuteur. 

D'abord sur la finalité : aujourd'hui un site web peut être un ensemble de documents (comme ce site) ou une application (comme GMail, Zoom, Facebook, etc.). Si les deux sont souvent appelés "sites web" et sont utilisés au sein d'un même logiciel, le navigateur (Firefox, Chrome, Edge, etc.), je parle bien ici d'un ensemble de documents et non d'une application.

Ensuite, je suis très attaché à la dimension amateure et communautaire du web, qui n'est pas du tout synonyme d'amateurisme. D'abord parce que j'aime l'idée que tout le monde soit autorisé à s'exprimer sur Internet. Mais aussi parce que cela génère souvent des démarches plus altruistes, qui produisent du contenu plus pertinent et original. Il existe même des [moteurs de](https://wiby.me/) [recherche](https://search.marginalia.nu/) dédiés à cette niche.

Si je suis [ingénieur en informatique](/cv.pdf), concevoir des sites web n'est pas mon métier (c'est d'ailleurs plusieurs métiers très différents : [Designer Graphique](https://www.onisep.fr/ressources/Univers-Metier/Metiers/designer-graphique), [Intégrateur Web](https://www.onisep.fr/ressources/Univers-Metier/Metiers/integrateur-integratrice-web), [Rédacteur Web](https://www.onisep.fr/ressources/Univers-Metier/Metiers/redacteur-redactrice-on-line), etc.) - je prétends juste avoir une bonne compréhension des aspects techniques. Si vous cherchez une prestation professionnelle, il vaut mieux prévoir un budget et vous adresser à une agence web.


Vous êtes encore là ? Bien ! En plus de la conception de votre site web, se pose la question de sa "mise en ligne". L'association [Deuxfleurs](https://deuxfleurs) dont je fais parti se chargera de cette partie. Son slogan "Fabriquer un Internet convivial" s'inscrit dans la continuité de notre démarche amateure. À travers [son blog](https://plume.deuxfleurs.fr/~/Deuxfleurs) et ses choix techniques, l'association interroge aussi la place qu'on veut accorder au numérique dans notre société.


## L'apparence

![Image d'illustration. Les artistes du jardin des plantes en 1902](/assets/images/pages/peintres.jpg)

Après cette introduction sur *le web amateur*, vous ne savez peut-être pas à quoi vous attendre. Pour vous faire une idée, le site web de l'association [Envie Appart'Agée](https://www.envieappartagee.fr/) est un exemple de ce qu'on peut créer avec [un outil facile](https://getpublii.com/) à prendre en main.


Si vous voulez pousser plus loin, certaines personnes ont aussi décidé d'apprendre [à coder des sites webs](https://openclassrooms.com/fr/courses/1603881-creez-votre-site-web-avec-html5-et-css3) (c'est beaucoup plus facile que ça en a l'air), ce qui permet de personnaliser le rendu comme on le souhaite. Ça peut être [très sobre](https://giraud.eu/), [très original](https://anneprudhomoz.fr/accueil.html), ou encore [très maitrisé](https://colineaubert.com/).

L'apparence de votre site web va influencer sa réception bien entendu, mais il n'a pas besoin d'être *beau*. En utilisant des [typographiques](https://fr.wikipedia.org/wiki/Typographie) avec [empatements](https://fr.wikipedia.org/wiki/Empattement_(typographie)), des [lettrines](https://fr.wikipedia.org/wiki/Lettrine), des [ornements](https://fr.wikipedia.org/wiki/Ornement_(typographie)), peu d'image, et une mise en page sobre, vous allez [signifier](https://visualdsgn.fr/semiologie-saussure-pierce-barthes/) que le contenu se rapporte à l'écriture, et par extension, à la sphère intellectuelle. Au contraire, si vous mettez des grandes images artificielles, des animations et autres procédés tape à l'œil, vous allez d'avantage signifier une vitrine de magasin. 

Aujourd'hui, les sites commerciaux étant dominants, c'est souvent eux qui définissent la norme de "ce à quoi devrait ressembler votre site web". Mais en réalité si vous êtes une association, ou que vous souhaitez partager un loisir, ça envoie des signaux contradictoires de ressembler à une vitrine...

On ne va donc pas trop se concentrer sur les normes esthétiques, mais on va faire attention aux signes qu'on mobilise.


## La motivation

![Image d'illustration. Gravure d'une femme assise à son bureau réfléchissant](/assets/images/pages/think.jpg)

Souvent la question du site web commence par une évidence : de nos jours, il faudrait être sur Internet ou risquer [une fracture numérique](https://books.openedition.org/pressesenssib/1940). C'est un mauvais point de départ selon moi car 1) tout n'a pas besoin d'être informatisé et 2) une [approche mimétique](https://fr.wikipedia.org/wiki/Culte_du_cargo) vise à reproduire quelque chose sans que ce soit nécessairement pertinent.

Un meilleur point de départ est de se demander ce que vous voulez dire, faire connaître et à qui. Et peut-être que vous n'aurez rien à dire (pour le moment), ou à des gens dont Internet n'est pas le média privilégié. Ce n'est pas grave, investissez votre énergie là où ça compte à la place. Autrement dit, votre site web doit faire partie d'une stratégie plus large de votre part : de communication, d'apprentissage, d'expression artistique, etc. 


<!--Il est fort probable qu'une part de votre public ait un smartphone ou un ordinateur. Et que vous ayez besoin de communiquer des informations basiques, comme une adresse, des évènements, des comptes sur les réseaux sociaux, etc. Et donc un site web pourrait faire partie de votre stratégie pour permettre aux gens de garder le contact avec vous et se tenir au courant de ce que vous faites.  Vous pourriez aussi simplement vouloir partager vos recettes de cuisine avec votre entourage. Ou partager des connaissances sur la permaculture avec vos paires maraîchers. Tout est possible, mais votre objectif final n'est pas le site web.-->

C'est donc bien de préciser votre objectif, car ça guidera vos choix d'une part, et d'autre part comment je vous aiderai. Si c'est une fin en soi, comme pour apprendre ou une démarche artistique, alors mieux vaut que vous fassiez un maximum par vous même, et que vous exploriez. Si c'est un moyen pour communiquer, alors mieux vaut s'assurer que vous êtes sur les bons rails. La suite de cette page prend le parti que votre site web est un support pour communiquer.

## La cible

![Image d'illustration. Une gravure de foule](/assets/images/pages/foule.jpg)

Avant de réfléchir à ce qu'on va écrire, on va réfléchir à votre cible : qui est t'elle et que cherche t'elle ? L'enjeu pour vous c'est de comprendre *dans quels contextes* les gens vont intéragir avec vous et/ou votre projet. Il y a souvent plusieurs contextes, et donc plusieurs cibles.

Prenons l'exemple d'une autrice. Elle peut vouloir adresser son site web à son lectorat. Mais elle pourrait aussi vouloir l'adresser à des maisons d'édition, à la presse, etc. Et elle pourrait vouloir aussi partager des conseils d'écriture : dans ce cas, la cible sont d'autres auteurs. Il vous revient donc de 1) recenser les différentes cibles et 2) identifier les informations pertinentes.

Pour certaines cibles, Internet ne sera pas pertinent du tout : soit parce qu'elles ne sont pas à l'aise avec cet outil, soit parce que consulter le web pour obtenir cette information précise n'est pas une évidence. Il est tout à fait possible de vouloir adresser son site web à une seule de ses cibles : c'est plus simple. Quand on a plusieurs cibles, c'est une stratégie de faire une page par cible (comme certains sites webs découpés en "pour les particuliers", "pour les entreprises", etc.).

## Le format

![Image d'illustration. Gravure d'une imprimerie](/assets/images/pages/imprimerie.jpg)

Si un site web peut être une page blanche sur laquelle votre [créativité](https://anaissiere.club1.fr/nuit/) [peut](https://estherbouquet.com/) [s'exprimer](https://anaissiere.club1.fr/amour/) [sans fin](https://melvynpharaon.club1.fr/myel/), avoir des point de référence pour commencer facilite le processus. Je vous propose 4 métaphores par ordre de difficulté pour expliquer les différentes directions que peut prendre votre site web : la *carte de visite*, la *brochure*, le *catalogue* et le *livre*.

La *carte de visite* est l'incarnation la plus simple d'un site web : une seule page qui donne des informations de contact traditionnelles (numéro de téléphone, adresse physique, adresse email) et/ou redirige vers vos réseaux sociaux. Elle s'adresse à des gens qui *vous connaissent déjà* et qui *savent déjà ce que vous faites* mais qui veulent retrouver des informations sur vous. Pour faire une carte de visite, vous n'avez besoin que de rassembler les informations de contact & de comptes de réseaux sociaux que vous voulez partager. Facile !

La *brochure* (ou portfolio), en plus des informations de contact, va donner des informations sur ce que vous faites et ou proposez. Elle peut donc s'adresser à des gens qui ont *entendu parler de vous*, *qui connaissent votre domaine* mais qui ne *savent pas exactement ce que vous faites*. Il faut donc lister vos activités : prestations, évènements, livres, formations, ateliers, œuvres, etc.

<!--Si vous organisez des évènements, ou que vous avez une actualité, vous pouvez penser une place à cette dernière - tout en gardant en tête que c'est une charge de travail conséquente de garder un site web à jour. Là aussi une seule page web suffit, par contre, par rapport à la carte de visite, elle demande un travail de production de contenu (rédaction de textes, photos, etc.).-->

Le *catalogue*, par rappport à la brochure, vise un nouveau public : *des gens qui ne vous connaissent pas* mais qui *connaissent votre domaine*. Ces gens vont donc chercher par mots clés, via un moteur de recherche (Google, Qwant, Lilo, etc.), des personnes comme vous. Pour être indexé dans le moteur de recherche, vous ne pouvez plus vous contenter de lister vos activités, il va falloir les décrire. Par exemple, si vous faites des formations sur le tricot, à qui ça s'adresse (débutants ?), ce qu'on va y apprendre (le point mousse ? le point jersey ?), ce qu'on va faire (tricoter une écharpe ?), etc.

Le *livre* est la forme la plus aboutie : c'est un site web qui contient des ressources qui se suffisent à elles-mêmes : des tutoriels, des guides pratiques, des fiches références, de l'actualité de votre domaine, etc. Il peut toucher des *gens qui ne vous connaissent pas* et *ne connaissent pas (encore) votre domaine*. Ça peut permettre de créer une communauté de gens qui suivent votre travail, un très fort engagement vis-à-vis de ce que vous faites, et vous placez en tant que référence dans votre domaine. Attention, ça demande *vraiment* beaucoup de travail, et le résultat n'est pas garanti : si vous tentez, faites le pour vous (motivation intrinsèque) et non parce que vous attendez des retombées (motivation extrinsèque).


Ces métaphores fonctionnent bien comme des poupées russes : la plupart des site webs ont une section contact, une large majorité parle de ce que la personne ou le collectif fait, un peu moins décrivent en détail ce qui est fait, et très peu fournissent des ressources clés en main en accès libre. Je vous recommande donc de commencer petit, et de jauger ce que vous étendez au fur et à mesure des retours, des envies et de votre temps. N'hésitez pas à réfléchir et à explorer d'autres métaphores physiques pour réfléchir au contenu que vous voulez partager.

Pour commencer, je vous recommande de partir des informations que l'on vous demande déjà souvent : vous savez qu'il y a déjà un intérêt pour ces dernières.

## Exister

![Image d'illustration. Une gravure de crieur](/assets/images/pages/crieur.jpg)

Contrairement aux réseaux sociaux qui ont des algorithmes de recommendation qui permettent de faire découvrir votre contenu sans action de votre part, ce n'est pas vraiment le cas des sites web. Une fois votre site web créé ce n'est donc pas terminé : il faut encore en assurer la promotion, du moins le faire connaître.

Pour faire connaître votre site web, il va falloir partager son adresse (ou URL).
L'adresse de mon site est `https://quentin.dufour.io`. Sur les navigateurs modernes, on peut n'entrer qu'une partie de l'URL, le nom de domaine : `quentin.dufour.io` (notez la disparition de `https://`), ce qui est plus simple à écrire et retenir. Beaucoup de personnes ne maîtrisent pas ces concepts et ont tout le temps recourt à la recherche par mots-clés dans un moteur de recherche (Google, Lilo, etc.), par exemple : `Quentin Dufour Informatique`. Il va falloir composer avec ces contraintes.

*Vous vous demandez peut-être comment obtenir un nom de domaine à votre identité et combien ça coûte ? Il faut compter environ 15 euros/an pour un .fr et il faut s'adresser à [un bureau d'enregistrement](https://www.afnic.fr/lexique/#bureaux-enregistrement) comme [Gandi](https://www.gandi.net/fr/domain). Ça fait partie de la technique qu'on peut faire ensemble.*

Sur vos documents écrits ou à l'oral, je vous conseille donc de prévoir d'indiquer votre nom de domaine (comme `quentin.dufour.io`). Si vous avez des raisons de penser que la personne va avoir du mal à vous trouver comme ça, il est bon de connaître les quelques mots-clés qui vont vous permettre de vous trouver à coup sûr dans un moteur de recherche mais...

...ça n'est pas si simple d'être référencé (indexé) dans un moteur de recherche. Il y a 2 choses principales à savoir : 1) ça prend du temps et 2) c'est une boite noire. Si tout se passe bien, votre site commencera a être référencé 1 mois après sa publication. Mais parfois, pour des raisons obscures, il peut ne pas être référencé du tout : il faut alors tatonner pour comprendre pourquoi. La compétition au référencement a créé [des métiers du référencement](https://www.onisep.fr/ressources/Univers-Metier/Metiers/charge-chargee-de-referencement-web) qui prétendent connaître les arcanes des moteurs de recherche et vous promettent monts et merveilles. Selon moi, plutôt que d'essayer de tromper le système, il est préférable de suivre les recommendations : 1) avoir d'autres sites web qui font des liens vers le votre, 2) publier du contenu original et de qualité et 3) publier régulièrement du contenu.

Diffuser l'existence de votre site web c'est bien, faire revenir votre public quand vous avez publié quelque chose de nouveau (des nouveaux évènements, une nouvelle actualité, de nouvelles informations, etc.) c'est encore mieux ! Si vous êtes sur les réseaux sociaux (Instagram, Facebook, etc.), vous pouvez annoncer cette mise à jour par ce biais, en mettant un lien. Si les groupes What's App, Signal, Telegram, ou autres sont populaires parmi vos cercles, ça peut être un autre moyen d'atteindre votre public. Aujourd'hui [les newsletters](https://www.radiofrance.fr/franceinter/podcasts/net-plus-ultra/net-plus-ultra-du-vendredi-22-septembre-2023-3758867) font aussi leur grand retour : l'email est loin d'être mort. Si vous faites parti de réseaux, vous pouvez essayer de compter sur eux pour relayer vos informations (office de tourisme, fédérations en tout genre, etc.) à travers leurs propres canaux de communication. 

Dans un premier temps, il n'est pas forcément nécessaire d'aller aussi loin : simplement mentionner à vos interlocuteurs que vous avez un site web est un bon début, et le reste sera du bonus !


## On se lance ?

![Image d'illustration. Une gravure de Montgolfière](/assets/images/pages/aerostat.jpg)


Vous avez maintenant toutes les cartes en main. Il ne vous reste plus qu'à prendre votre cahier ou [votre logiciel de traitement de texte](https://fr.wikipedia.org/wiki/Logiciel_de_traitement_de_texte) préféré,
et coucher vos idées sur la papier (ou sur l'écran). 
Voici un récapitulatif des éléments auxquels vous devez réfléchir - quelques lignes peuvent suffire :
 - Dans quelle stratégie s'inscrit votre projet de site web ?
 - Quelle est votre cible (ou quelles sont vos cibles) et que cherchent t'elles selon vous ?
 - Quel format souhaitez-vous adopter pour votre contenu (carte de visite, brochure, catalogue, etc.) ?
 - En fonction du format que vous voulez, écrire les blocs de texte qui apparaîtront
 - Rassembler des images, si nécessaire, qui serviront à illustrer votre site
 - Si vous avez déjà des supports de communication, les rassembler aussi : on pourra les réutiliser pour créer du contenu.
 - Notez quelques idées sur comment vous allez faire connaître votre site web
 - Réfléchissez au nom de domaine que vous voudriez

Je suis conscient que ça fait beaucoup de choses, mais elles me semblent essentielles pour la réussite de votre projet. Rien ne presse : si vous n'avez pas le temps maintenant, ça peut être plus tard.

Une fois prêt·e, on peut planifier une demi-journée ensemble pour mettre en place la partie technique. Vous pouvez trouver mes informations de contact sur [ma page d'accueil](https://quentin.dufour.io).

