---
layout: post
slug: vous-avez-dit-os
status: published
sitemap: true
title: Le concept d'OS
description: Comprenez votre ordinateur
category: developpement
tags:
---

![Bureau Ubuntu](/assets/images/posts/ubuntu-desktop-1.png)

Les systèmes d'exploitation ou OS sont au coeur de notre vie. Par contre, on a souvent un peu de mal à comprendre ce que c'est.
Posons quelques bases.

## Comprendre ce qu'est un système d'exploitation

![Logo de Windows, macOS et Ubuntu](/assets/images/posts/ubuntu-os-list.png)

Windows, macOS et Ubuntu, dont les logos se trouvent juste au dessus, sont des Systèmes d'Exploitation. L'abbréviation anglaise OS pour Operating System est assez courante : c'est elle que vous retrouvez dans les termes macOS ou iOS (pour les iPhone). 

Vous n'avez probablement jamais eu à vous soucier d'eux car très souvent ils sont livrés avec l'ordinateur que vous achetez.
Souvent, c'est Windows que vous retrouverez, sauf pour les ordinateurs Apple qui sont livrés avec macOS.
Et Ubuntu ? On peut trouver certains ordinateurs vendus avec (chez Dell ou des constructeurs spécialisés, comme System76 ou Purism) mais ça reste marginal.

*Au passage, lors de l'achat de votre ordinateur, vous payez pour Windows. Alors que vous pourriez vouloir utiliser Ubuntu à la place, qui est gratuit. Théoriquement, vous pourriez demander un remboursement. En pratique, c'est plus compliqué... Pour plus d'information, regardez du côté de la vente liée.*

Votre ordinateur est donc très souvent livré avec un système d'exploitation pour des raisons pratiques. Mais un système d'exploitation n'est qu'un logiciel, du code, une sorte de super application, il n'a pas d'existence matériel. Cette super application est lancée au démarrage de votre ordinateur. Elle est constituée d'un ensemble de fichiers stockés sur votre disque dur, au même titre que le dernier tube de Rihanna ou cette formidable grimace que vous avez faite à votre webcam. Pour changer de système d'exploitation, il suffit d'en réécrire un nouveau sur votre disque.

![Le système d'exploitation est stocké sur le disque dur](/assets/images/posts/ubuntu-se-dd.png)

Avant de voir pourquoi en changer, essayons de comprendre à quoi il sert. Le matériel informatique est très bête : votre processeur ne sait qu'exécuter une liste d'instructions, votre disque dur stocker des 0 et des 1 et votre écran afficher des pixels. D'ailleurs, en fonction du disque dur ou de l'écran, le fonctionnement sera différent. Un développeur voulant écrire une application sans système d'exploitation se heurtera donc à un problème de **généricité** : il devra réécrire son code à chaque fois qu'une pièce change. 

Maintenant, soyons fou et demandons l'impossible, exécutons deux applications en même temps : nouveau problème, il va falloir **partager les ressources**. Où je vais écrire mes fichiers sur le disque dur pour que l'autre application ne les écrase pas, est-ce que je peux utiliser cette partie de l'écran, etc. On résout ce problème en déléguant ces décisions au système d'exploitation.
 
Contrôle : 

![Le système d'exploitation est un intermediaire entre les apps et le materiel](/assets/images/posts/ubuntu-se.png)

**Pour résumé, le système d'exploitation est un super logiciel stocké sur le disque dur de l'ordinateur. C'est le premier lancé au démarrage. Il va se placer entre le matériel, dont il va prendre le contrôle, et les applications, à qui il va partager ce matériel, contrôler leur usage et fournir une boite à outils pour le manipuler aisément.**

