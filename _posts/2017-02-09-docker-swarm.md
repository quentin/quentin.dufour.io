---
layout: post
slug: decouverte-docker-swarm
status: published
title: Docker Swarm
description: Toujours plus de Docker
category: operation
tags:
---


## Tentative Scaleway

Etant donné le coup très faible des machines Scaleway, j'ai tenté de provisionné plusieurs machines pour réaliser un swarm. Mais ça a été plus compliqué que prévu.

 * [Impossible d'initiliaser le manager](https://community.online.net/t/docker-swarm-cant-initialize-a-manager-in-1-12-using-native-swarm-commands/2832)

Pour contourner ce problème, il faut accepter d'utiliser les IP privées, et donc déclarer votre ip en `10.x.x.x`.

 * [Modules noyaux nécessaires pour le mesh Swarm](https://github.com/docker/docker/issues/28168)
 * [Problèmes avec les bootscripts Scaleway](https://github.com/scaleway/image-ubuntu/issues/78)

Apparemment les noyaux Scaleway ne supportent pas toujours les modules attendus par Docker Swarm.
Mais il semble que le noyeau de l'image Docker devrait fonctionner.
D'ailleurs, une rapide vérification avec le script ci-dessous nous en assure.

Malheureusement, malgrés toutes ces précautions, impossible d'avoir un swarm fonctionnel.
Ainsi, une fois le docker registry lancé dans le swarm :

```
$ curl -v 172.18.0.3:5000/v2/_catalog # soit l'IP du conteneur
*   Trying 172.18.0.3...
* Connected to 172.18.0.3 (172.18.0.3) port 5000 (#0)
> GET /v2/_catalog HTTP/1.1
> Host: 172.18.0.3:5000
> User-Agent: curl/7.47.0
> Accept: */*
>
< HTTP/1.1 200 OK
< Content-Type: application/json; charset=utf-8
< Docker-Distribution-Api-Version: registry/2.0
< X-Content-Type-Options: nosniff
< Date: Thu, 09 Feb 2017 13:58:15 GMT
< Content-Length: 20
<
{"repositories":[]}

$ curl -v localhost:5000/v2/_catalog
*   Trying ::1...
* Connected to localhost (::1) port 5000 (#0)
> GET /v2/_catalog HTTP/1.1
> Host: localhost:5000
> User-Agent: curl/7.47.0
> Accept: */*
> 
^C

```

## Ailleurs

Et pour vérifier que votre noyau supporte bien tout ce qu'il vous faut :

```
bash <(curl https://raw.githubusercontent.com/docker/docker/master/contrib/check-config.sh -Lk)
```

## Installer Docker rapidement sous Debian

Voici un script réalisé à partir des commandes fournies par la documentation de Docker :

```
#!/bin/bash
apt update

apt install -y curl \
  apt-transport-https \
  ca-certificates \
  software-properties-common

curl -fsSL https://yum.dockerproject.org/gpg |
  apt-key add -

add-apt-repository "deb https://apt.dockerproject.org/repo/ debian-$(lsb_release -cs) main"

apt update
apt -y install docker-engine
docker -v
```

## Créer le swarm

* [Tutoriel Docker Swarm](https://docs.docker.com/engine/swarm/swarm-tutorial/)

Maintenant sur master :

```
docker swarm init --advertise-addr 1.2.3.4
```

Ce qui devrait donner quelque chose comme :

```
Swarm initialized: current node (....) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token ... 1.2.3.4:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

On va donc lancer la commande sur les 2 slaves.
*Si vous avez perdu la commande, tapez juste `docker swarm join-token worker` sur le master*.

On vérifie que tout s'est bien passé :

```
$ docker node ls
ID                           HOSTNAME   STATUS  AVAILABILITY  MANAGER STATUS
6wyl4ozytbgdeak4s6sahs2zo *  rincevent  Ready   Active        Leader
i2lg4r3nsnvl9i8s0rr3t4fcy    insalan    Ready   Active        
```

Pour quitter le swarm, la commande est :

```
docker swarm leave --force
```

*Le --force est seulement nécessaire pour les managers*

## Lancer un docker registry pour stocker nos images

 * [Déployer un docker registry](https://docs.docker.com/registry/deploying/)
 * [Tutoriel Docker Registry sur Swarm](https://lostechies.com/gabrielschenker/2016/09/05/docker-and-swarm-mode-part-1/)

Dans notre cas, on va lancer le registry sans authentification et sans certificats TLS. De ce fait, il ne sera accessible que depuis localhost. L'idée c'est qu'avec le network mesh de Docker Swarm, il sera accessible depuis tous les noeuds du swarm via `localhost:5000`. Bien entendu en production, il serait préférable d'avoir un registry bien configuré...

```
docker service create --replicas 1 --publish 5000:5000 --name registry registry:2
```

Quelques commandes pour voir si tout s'est bien passé :

```
docker service ls
docker service inspect --pretty registry
docker service ps registry
```

Pour tester que tout marche bien :

```
curl localhost:5000/v2/_catalog
```

Et si besoin de le supprimer :

```
docker service rm registry
```

## Constuire nos images et les publier sur le registry

Pour chaque image, on va la build sur un des serveurs du swarm (car le repository n'est accessible que depuis localhost) et la publier :

```
cd mon_image/
docker build -t mon_image .
docker tag mon_image localhost:5000/mon_image
docker push localhost:5000/mon_image
```

## En conclusion

Après un échec de déploiement sur Scaleway et une réussite en demi teinte en local (le network mesh ne marchant pas sur mon Docker dans ma LXC), je vais en rester là pour le moment, en attendant de faire des machines virtuelles ou d'avoir du materiel pour tester.
