---
layout: post
slug: cloner-vers-un-disque-plus-petit
status: published
title: Migrer vers un disque plus petit
description: Migrer vers un SSD sans tout réinstaller
disqus: true
category: operation
tags:
---

J'ai un peu cherché sur internet comment migrer vers un SSD plus petit que le disque dur d'origine sans trouvé de vrai guide détaillé. Souvent moqué, c'est une idée loin d'être bête. En effet, certains de mes PC n'ont besoin que de 250Go d'espace disque mais ont de vieux disques dur de 500Go. Autant migrer sur des SSD de 250 Go ! 

Dans mon cas, j'ai effectué cette procédure sur 2 PCs sous Windows 10, avec 2 ou 3 partitions (System Reserved - des fois, la partition C:, et la partition diag).

## Avertissement !

Je ne suis pas responsable si vous perdez des données. Il existe de nombreuses raisons pour que les opérations décrite après se passent mal : cas particulier, erreur matériel, perte de courant, erreur humaine, etc.

Nous allons utiliser l'outil `dd` en ligne de commande qui est loin d'être un outil user friendly. Inversez deux paramètres et vous vous retrouverez à copier le contenu de votre SSD vide sur votre disque dur. C'est ce qui lui vaut le doux surnom de *Data Destroyer*.

Vous ne devriez tenter le coup qu'après avoir sauvegardé toutes les données importantes sur un autre support qui sera déconnecté lors de la manipulation ainsi qu'avoir de quoi réinstaller un système complet en cas d'échec (et le temps de le faire !).

## Étape 1 - Bien se préparer

Dans notre cas on va utiliser la dernière version de [Fedora 25 Workstation](https://getfedora.org/fr/workstation/download/). Vous pourrez probablement adapter ce tutoriel avec n'importe quelle distribution live. 

*Le choix d'une distribution générique est critiquable sachant que des distributions gparted live existent, mais elle permet d'éviter certaines déconvenues comme le support de plus de materiels, mieux testées, plus polyvalente en cas de problème, etc.*.


Une fois votre image téléchargée, vous devez "l'écrire" sur une clé USB (ou un DVD).
Depuis Windows, je vous conseille l'outil [Rufus](https://rufus.akeo.ie/).
Depuis Linux ou Mac OS, nous allons utiliser `dd`.
*Si vous êtes sous Fedora, vous pouvez utiliser l'outil Disques (Disks en anglais, gnome-disks depuis la cli) pour trouver le path de votre clé USB - ici `/dev/sdb`*

```
sudo dd if=image.iso of=/dev/sdb bs=4M
```


Avant de commencer, il est important de laisser votre partition NTFS en bon état. Et donc d'éteindre complètement votre ordinateur.

*En effet, par défaut depuis Windows 8, windows mets en cache et verrouille la partition NTFS quand vous cliquez sur éteindre pour permettre un démarrage plus rapide. Il existe d'autres raisons pour que votre partitiion soit verrouillée. Vous vous en rendrez compte, si ça vous arrive, au moment de redimensionner la partition avec gparted : vous ne pourrez pas et cette dernière présentera un panneau avertissement devant.*

```batch
shutdown /s /t 0
```

Branchez aussi votre SSD sur l'ordinateur. Afin de me simplifier la vie et ne pas m'amuser à trouver un autre cable SATA et d'alimentation, j'ai branché le SSD en USB sur l'ordinateur avec le contrôleur que j'ai récupéré d'un disque dur externe. Si vous avez un boitier disque dur externe avec possibilité de mettre votre propre disque, c'est la même chose.

## Étape 2 - Configurer l'instance live de Fedora

On va boot sur la clé USB Fedora live 25. Le démarrage peut être un peu long.

Notre première étape sera de passer notre clavier en français azerty (ou n'importe quel autre format en fait).
Depuis les paramètres, on va choisir "Région et Langues" puis ajouter une "Input Source"

[![](/assets/images/posts/cloner-param.png)](/assets/images/posts/cloner-param.png)
[![](/assets/images/posts/cloner-param2.png)](/assets/images/posts/cloner-param2.png)

Ensuite on va installer gparted :

```
sudo dnf install -y gparted
```

## Étape 3 - Diminuer la taille des partitions

Nous allons commencer par lancer gparted

```
sudo gparted
```

Assurez-vous que le disque selectionné est le bon (en haut à droite).
On va redimensioner nos partitions de tel sorte à avoir l'ensemble de nos partitions plus petite que le disque de destination au début du disque.
Ainsi, si votre disque cible fait 250 Go, essayez de n'avoir que les 200 premiers Go d'occupés sur votre disque, le reste devant être vide.

[![](/assets/images/posts/cloner-resize.png)](/assets/images/posts/cloner-resize.png)

Si on se laisse une marge de manoeuvre, c'est pour éviter les erreurs de calculs liées aux divers unités utilisées, aux tailles de blocs, etc. Pas d'inquiétude, on étendra la partition une fois copiée pour utiliser tout l'espace à disposition.

Vous aurez peut être à bouger des partitions, apparemment ça peut poser des problèmes. A vous de faire attention !

## Étape 4 - Copier le contenu du disque sur le SSD

Utilisez l'utilitaire de disque de Fedora pour facilement identifier vos disques.

[![](/assets/images/posts/cloner-disk2.png)](/assets/images/posts/cloner-disk2.png)
[![](/assets/images/posts/cloner-disk3.png)](/assets/images/posts/cloner-disk3.png)

Ici, on va copier notre disque dur `/dev/sda` vers le SSD `/dev/sdg` avec une taille de bloc de `4M` et on va afficher l'avancement avec `status=progress`.

```bash
sudo dd if=/dev/sda of=/dev/sdg bs=4M status=progress
```

Si vous avez oublié le progress, vous pouvez afficher l'avancement en tapant dans un autre terminal :

```bash
ps aux | grep dd # relevez le PID, ici on suppose que c'est 2534
sudo watch -n 60 kill -USR1 2534 # watch execute la commande toutes les 60 secondes
```

Vous allez finir avec une erreur car le disque cible est plus petit. C'est normal, enfin attendu.
*Si on voulait faire ça bien, on aurait utilisé `count` pour copier le bon nombre de blocs. C'est un calcul entre la taille du disk, la taille des blocs et le nombre de blocs copiés. On se serait alors arrêté de copier au bon moment.*

```bash
sudo sync
```

Pour être sûr qu'on a bien écrit les données sur le disque.

## Étape 5 - Augmenter la taille des partitions pour combler l'espace vide

On va relancer gparted pour agrandir nos partitions pour utiliser au maximum l'espace disponible. Sachez que si vous n'avez pas de partitions à déplacer, vous pourriez directement augmenter la partition C: depuis Windows dans l'outil de gestion de disque, même si Windows est en train de l'utiliser - bien que je n'ai pas tenté !



## Étape 6 - Finaliser votre installation

Il nous reste encore quelques actions à réaliser pour finaliser notre opération :

 * Installer le SSD à la place du disque dur
 * Fixer les éventuelles erreurs trouvées par Windows
 * Garder votre ancien disque dur en l'état un petit peu afin de vérifier que tout fonctionne bien.

*Lorsque j'ai remplacé le disque dur par le SSD, je me suis rendu compte que le BIOS d'un de mes ordinateurs ne reconnaissait pas le SSD... Heureusement, j'avais une autre tour inutilisée, mais à ce jour je n'ai pas de solution. D'autant plus que ce dernier refusait aussi de détecter ma carte SATA en PCI*
