---
layout: post
slug: resoudre-vos-problemes-de-connexion-internet
status: published
title: Optimiser son ADSL
description: Prise téléphone, condensateur et court-circuits
disqus: true
category: operation
tags:
---


<p>Vous avez des problèmes avec votre connexion internet, divers bugs, du genre :
"ma box plante", "Je perds ma connexion internet tous les soirs" ou encore "Mon débit est limité".
Vous n'êtes pas le seul, j'ai également vécu cette situation. Voilà quelques solutions qui vous aideront (peut-être).</p>
<h2>Ma connexion internet n'est pas stable</h2>
<p>Vous perdez régulièrement votre connexion internet ? Le soir en particulier ? Il s'agit de pertes de synchronisations. Ces dernières proviennent en général de la mauvaise qualité de réception du signal. Soit de votre installation personnelle, soit de l'installation France Telecom. Dans le deuxième cas il ne reste pas grand chose à faire.</p>
<p>Il est préférable de brancher votre routeur sur la prise téléphonique principale de votre maison et de limiter au maximum les dérivations qui partent de cette dernière qui pourraient la parasiter.</p>
<p>Vérifiez d'abord votre prise téléphonique. Est-elle oxydée à l'intérieur ? Et les branchements fait dessus sont-ils correctement réalisés ? Votre prise ne nécessite que deux fils de branché, le blanc et le gris. Faites attention aux branchements qui en partent. Vous pouvez essayer en ne laissant que ces deux fils branchés.</p>
[![Schéma prise téléphonique](/assets/images/posts/telephone-01.png)](/assets/images/posts/telephone-01.png)
*Source : [Cablage prise de téléphone en T - josdblog](http://gurau-audibert.hd.free.fr/josdblog/2012/06/cablage-prise-telephone-en-t/)*
<p>Faites attention à l'âge de votre prise, car les contacts entre la prise et la fiche sont très mauvais. Les connecteurs de la prise femelle sont trop souples, et si il ne sont pas absolument neufs et pas oxydés, vous tenez surement la cause de vos déconnexions intempestives !</p>
<h2>Mon débit est limité (abonnés Orange uniquement)</h2>
<p>Si votre débit est inférieur à celui auquel vous êtes éligible, il est peut-être bridé par un DLM (Dynamic Line Management) mis en place automatiquement par Orange. Ce dernier mesure la qualité de votre ligne, et si il considère qu'elle n'est pas assez stable, il bride le débit de votre ligne (entre autre). Si vous considérez que vous n'en avez pas besoin, vous pouvez formuler une demande afin que ce bridage soit retiré <a href="http://assistance.orange.fr/contact_dlm.php">à partir de ce formulaire.</a></p>
<h2>Compléments et sources</h2>
<a href="http://telephoniste.free.fr/circuits/priseT/">Informations sur les prises téléphoniques</a><br/>
<a href="http://assistance.orange.fr/vos-questions-sur-le-dlm-4312.php">Explication du DLM par Orange</a>

