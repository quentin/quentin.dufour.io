---
layout: post
slug: latex-dvi-integrer-image
status: published
sitemap: true
title: Latex, DVI et images
description: Grâce à ImageMagick
category: developpement
tags:
---

Pour les compilateurs LaTeX compilant vers le format DVI-Tex (DeVice Independent) / PostScript, 
il n'est pas possible d'intégrer directement des images au format JPG, PNG ou autre.
Si aujourd'hui le plus simple est d'utiliser un compilateur LaTeX qui produit des fichiers au format PDF et supporte les formats d'image précédents,
ce n'est pas toujours possible.

Dans ce guide, nous supposons donc que nous souhaitons garder une sortie DVI-Tex / PostScript.
Pour ce faire, nous utiliserons l'outil `convert` du projet [ImageMagick](https://imagemagick.org/index.php) (fiche [Wikipedia](https://fr.wikipedia.org/wiki/ImageMagick))
pour convertir préalablement les images au format EPS.

## Installer ImageMagick

Si la commande `convert` n'est pas disponible sur votre système, vous allez devoir installer ImageMagick.

Sur Fedora :

```bash
sudo dnf install ImageMagick
```

Sur Ubuntu :

```bash
sudo apt update
sudo apt install imagemagick
```


## Notre image de test

Nous allons utiliser [une photo](https://quentin.dufour.io/assets/images/posts/dijkstra.jpg) de [Dijkstra](https://fr.wikipedia.org/wiki/Edsger_Dijkstra) pour nos tests, que vous pouvez télécharger comme suit :

```bash
wget https://quentin.dufour.io/assets/images/posts/dijkstra.jpg
```

## L'outil convert

`convert` peut être utilisé sans paramètre, mais dans notre cas le résultat de la conversion sera décevant :

```
convert dijkstra.jpg dijkstra.eps
```

`convert` a en effet de nombreux paramètres qui sont tous décrits en détails dans le manuel (`man convert`) dont les valeurs par défaut ne sont pas toujours appropriées.
Ici nous nous penchons seulement sur les deux problèmes suivants :
  - Les images sont trop grandes et pixelisées à cause d'une valeur par défaut inadéquate du paramètre `density`
  - Les images sont trop lourdes car aucune compression n'est utilisée pour la sortie

Les images sont représentées sous forme de pixels mais les sorties LaTeX raisonnent en centimètres: il faut donc choisir combien de pixels on met par centimètres.
On parle souvent de [DPI](https://fr.wikipedia.org/wiki/Point_par_pouce) pour ce problème, ImageMagick utilise le terme de densité.
La valeur de densité par défaut de `convert` est très basse (probablement autour de `96` pour des raisons historiques).
D'expérience, je recommande une valeur entre `172` et `300` pour éviter de se retrouver avec une image pixelisée, à ajuster en fonction de la taille voulue.

Ensuite, historiquement, le format EPS ne supporte pas de compression et produit des images très lourdes par défaut.
Notre image originale au format JPG fait `846 ko` ; convertie sans compression, sa taille grimpe à `12 Mo`.
Au delà de la taille sur le disque, une telle taille est problématique car elle peut faire planter la compilation ou la visionneuse.
Heureusement, [StackOverflow](https://stackoverflow.com/questions/5350246/convert-jpg-to-eps-format) nous apprend que des évolutions du format existent : ces dernières ont l'avantage de supporter la compression.
Pour les utiliser il suffit de préfixer notre fichier de sortie par `eps2:` ou `eps3:` (exemple: `eps2:dijkstra.eps`).
En choisissant un format ou l'autre, on revient sur une taille similaire au JPG d'origine, de `841 ko`.

La commande finale que je recommande pour la conversion est donc :

```bash
convert -density 300 dijkstra.jpg eps2:dijkstra.eps
```

## Intégrer l'image dans un document LaTeX

Je crée un fichier LaTeX très simple nommé `trombi.tex`

```latex
\documentclass{article}
\usepackage{graphicx}
\begin{document}
\includegraphics{dijkstra.eps}
\end{document}
```

Que je compile ensuite :

```
latex trombi.tex
```

Et que je peux ouvrir avec :

```
xdg-open trombi.dvi
```

Et voici le résultat !

![Dijkstra dans Evince](/assets/images/posts/dijkstra-res.png)
