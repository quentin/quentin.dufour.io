---
layout: post
slug: installer-icescrum-7
status: published
title: Installer Icescrum
description: Gestion de projet agile self-hosted
category: operation
tags:
- projet
- developpement
---

Le processus d'installation sera décrit dans le cas d'une installation Debian normale. Si vous utilisez une autre distribution, vous devrez peut-être adapter certaines commandes. Tous les processus d'installation seront en cli (interface ligne de commande). Je ne suis pas un expert java ni sysadmin, certains choix ne seront peut-être pas les meilleurs. Je suis ouvert à vos retours.

__Il sera considéré que vous possédé déjà une installation de java sun complète et fonctionnelle.__

### Installation via apt-get

```bash
$ sudo apt-get install tomcat7 tomcat7-common mysql mysql-client mysql-java
```

_Nous n'utiliserons pas l'interface web de configuration disponible avec tomcat_
Une fois installé, vérifiez votre installation en allant sur http://localhost:8080
Vous devriez voir le It Works de tomcat.

### Téléchargement de IceScrum 7

Nous allons maintenant pouvoir télécharger le fichier icescrum.war sur [le site officiel](http://www.icescrum.org/download/). Attention, le lien est un tout petit bouton vert.

Considérons que vous l'avez place dans le home de l'utilisateur courant, déplacez le :

``` bash
$ sudo mv ~/icescrum.war /var/lib/tomcat7/webapps/
```

### Configuration Tomcat

N'ayant pas réussi à utiliser le système de base de donnée fourni avec icescrum et qui plus est déconseillé pour une autre utilisation que des tests, je vous propose une configuration avec MySQL. Libre à vous d'adapter votre configuration en fonction de vos besoins. Tout d'abord Icescrum a besoin de variables d'environnement pour fonctionner.

```bash
$ sudo nano /usr/share/tomcat7/bin/setenv.sh
```

Il ne vous reste plus qu'à y insérer le contenu suivant :

```bash
CATALINA_OPTS="-XX:MaxPermSize=512m -Xmx1024m"
CATALINA_OPTS="$CATALINA_OPTS -Dicescrum_config_location=/etc/tomcat7/icescrum.groovy"
CATALINA_OPTS="$CATALINA_OPTS -Duser.timezone=UTC"
CATALINA_OPTS="$CATALINA_OPTS -Dicescrum.log.dir=/var/lib/tomcat7/basedir/icescrum/"
```

Avant d'entrer vraiment dans le vif de la configuration de Icescrum, vous devrez tout d'abord configurer votre serveur tomcat pour qu'il puisse supporter icescrum, pour ce faire :

```bash
$ sudo nano /etc/tomcat7/server.xml
```

Trouvez la ligne commencant par `<Connector port="8080"` et remplacez là entièrement par `<Connector port="8080" protocol="org.apache.coyote.http11.Http11NioProtocol" connectionTimeout="2000" maxThreads="500" URIEncoding="UTF-8"/>`

### Configuration Mysql

Il faut ajouter une base de donnée vide à votre mysql. On l'appelera ici icescrum.

```bash
$ echo "create database icescrum"|mysql -u root -p
```

### Configuration Icescrum

Vous devez créer maintenant le fichier de configuration principal de icescrum

```bash
$ sudo nano /etc/tomcat7/icescrum.groovy
```

et insérez le texte suivant après avoir configuré ce qui vous intéresse.

__Attention, grails.serverURL est très important, il doit représenter l'url du serveur tel qu'il sera vu par les clients, n'oubliez pas d'éditer si vous faites des modification !!!__

```text
//Server URL - IMPORTANT
grails.serverURL = "http://127.0.0.1:8080/icescrum"

//Logging (for debug purposes)
icescrum.debug.enable = true

//Working directory
icescrum.baseDir = "/var/lib/tomcat7/basedir/icescrum/"

//PostgreSQL
//dataSource.driverClassName = "org.postgresql.Driver"
//dataSource.dialect="net.sf.hibernate.dialect.PostgreSQLDialect"
//dataSource.dialect="org.hibernate.dialect.PostgreSQLDialect"
//dataSource.url = "jdbc:postgresql://localhost:5432/icescrum"
//dataSource.username = "postgres"
//dataSource.password = "postgres"

dataSource.dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
dataSource.driverClassName = "com.mysql.jdbc.Driver"
dataSource.url = "jdbc:mysql://localhost:3306/icescrum?useUnicode=true&characterEncoding=utf8"
dataSource.username = "root"
dataSource.password = "password"

//Mail server (These exemple values aren't set by default)
grails.mail.host = "smtp.gmail.com"
grails.mail.port = 465
grails.mail.username = "identifiant@gmail.com"
grails.mail.password = "mot-de-passe"

//Project
icescrum.project.import.enable = true
icescrum.project.export.enable = true
icescrum.project.creation.enable = true
icescrum.project.private.enable = true

//Users
icescrum.gravatar.secure = false
icescrum.gravatar.enable = true
icescrum.registration.enable = true
icescrum.login.retrieve.enable = true

//Alerts
icescrum.auto_follow_productowner = true
icescrum.auto_follow_stakeholder = true
icescrum.auto_follow_scrummaster = true
icescrum.alerts.errors.to = "quentin@dufour.tk"
icescrum.alerts.subject_prefix = "[icescrum]"
icescrum.alerts.enable = true
icescrum.alerts.default.from = "mrsuperboum@gmail.com"

//Attachments
icescrum.attachments.enable = true
```

Il faut aussi créer les dossiers que l'on a déclaré dans le setenv.sh précédemment

```bash
$ sudo mkdir -p /var/lib/tomcat7/basedir/icescrum
```

Puis donner les permissions à tomcat dessus :

```bash
$ sudo chown -R tomcat7:tomcat7 /var/lib/tomcat7/basedir
```

### Lancement

Il ne vous reste plus qu'à relancer tomcat et croiser les doigts.

```bash
$ sudo service tomcat7 restart
```

Pour voir si tout se passe bien, regardez les logs de tomcat :

```bash
$ sudo tail -f /var/log/tomcat7/catalina.out
```

Enfin, vous pouvez accéder à votre installation icescrum depuis [http://127.0.0.1:8080/icescrum](http://127.0.0.1:8080/icescrum)

Si vous rencontrez des problèmes, commencez par vérifier les permissions sur vos répertoires et que tout ce que vous deviez installer à bien été installé !
