---
layout: post
slug: utiliser-casperjs
status: published
title: Aperçu de CasperJS
description: Du test fonctionnel au web scraping
category: developpement
tags:
- projet
- developpement
---

CasperJS est un outil qui permet d'écrire des tests fonctionnels sur un projet web. Il se charge de tester votre application web en général, et suitune liste d'action prédéfinie. Il teste la stabilité et la cohérence général du service. C'est super pratique quand il s'agit de détecter les effets de bord !

```javascript
casper.start('http://my_website.tld/login', function() {
  this.test.assertTitle('Super website', 'Web page title OK');
  this.sendKeys('form[action*="login_check"] input[id="username"]', 'me@domain.tld');
  this.sendKeys('form[action*="login_check"] input[id="password"]', 's3cr3t');
  this.click('form[action*="login_check"] input[type="submit"]');
});

casper.then(function(){
 console.log('Url courante : ' + this.getCurrentUrl());
 this.test.assertSelectorHasText('#content-header > h1', 'Tableau de bord', 'Web page main content OK');
});

casper.run(function() {
  this.test.done();
});

 ```

## Fonction 1
Ce script se charge de se rendre sur la page de connexion, vérifie le titre de la page puis entre un identifiant et un mot de passe et enfin valide.

## Fonction 2
La seconde fonction vérifie que la page contient bien ce qui est attendu après la connexion. Ici, un titre h1 contenant le texte Tableau de bord.

## Fonction 3
Enfin cette dernière fonction indique que le test est fini. Elle affiche les statistiques sur le test.
