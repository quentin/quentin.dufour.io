---
layout: post
slug: recharger-carte-korrigo-sur-fedora
status: published
sitemap: true
title: Recharger sa carte Korrigo sur Fedora
description: C'est possible !
disqus: false
category: operation
tags:
- fedora
---

La STAR, la Société de Transport de l'Agglomération Rennaise, propose à la vente des lecteurs de carte pour pouvoir recharger votre carte depuis chez vous. Pour cela, elle a fait le choix d'utiliser une application Java Web Start, qui fonctionne sous Windows, Mac OS et... Linux ! Pour autant, la procédure n'est pas totalement directe.

## Installer les dépendances

```
sudo dnf install pcsc-lite-ccid pcsc-tools icedtea-web
```

Si vous rencontrez des problèmes à l'étape "Utilisez l'applet Java Web Start", vous pouvez essayer d'installer le Java d'Oracle. Rendez-vous sur la page [de téléchargement d'Oracle](https://java.com/en/download/linux_manual.jsp) et choisissez Linux x64 RPM (ou Linux RPM si vous avez une installation 32 bits).

Une fois téléchargé, executé la commande suivante dans le répertoire de téléchargement :

```
sudo dnf install jre-8u151-linux-x64.rpm
```

Puis remplacez par la suite toutes les occurences de javaws par :

```
/usr/java/jre1.8.0_151/bin/javaws
```

## Test du lecteur

Vous devez probablement être dans le groupe dialout pour utiliser votre lecteur USB :

```
sudo usermod -a -G dialout votre_nom_d_utilisateur
```

Vous allez également devoir démarrer le daemon pcscd :

```
sudo systemctl start pcscd
```

Vérifiez que votre carte est bien détectée en lançant :

```
pcsc_scan -n
```

## Utiliser l'applet Java Web Start

Avant toute chose, il faut savoir que Java cherche la lib sous le nom de libpcsclite.so et non pas libpcsclite.so.1. Nous allons donc exécuter la commande suivante :

```
sudo ln -s /usr/lib64/libpcsclite.so.1 /usr/local/lib64/libpcsclite.so
```

Ensuite, connectez-vous sur votre compte STAR, et cliquez sur "Lire ma carte".
Puis sur le bouton commencer, le site va vous proposer de télécharger un fichier en .jnlp. Une fois téléchargé, exécutez le comme suit :

```
javaws Application-KorriGo-STAR.jnlp
```

Normalement le site va détecter votre lecteur puis votre carte.

Bon courage !
