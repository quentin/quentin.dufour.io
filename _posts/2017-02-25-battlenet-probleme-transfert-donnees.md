---
layout: post
slug: battlenet-probleme-transfert-donnees
status: published
title: "Debugguer Battle.net"
description: Et quelques outils de debuggage Windows
category: operation
tags:
---

J'ai rencontré un bug assez coriace sur le client Battle.net. Cet article peut vous intéresser si vous aussi vous rencontrez l'erreur suivante :

> Nous rencontrons un problème de transfert des données. Veuillez vérifier l'état de votre connexion Internet et réessayer.

Et en image (cliquez dessus pour l'agrandir) :

[![Erreur battlenet](/assets/images/posts/battlenet-erreur.png)](/assets/images/posts/battlenet-erreur.png)

## La solution

Pour ceux qui seraient pressés, il suffit d'arrêter toutes les instances de Battle.net puis de supprimer les dossiers suivants :

 * C:/Program Files/Battle.net/ ou C:/Program Files (x86)/Battle.net/
 * C:/ProgramData/Battle.net/
 * C:/ProgramData/Blizzard Entertainment/
 * C:/Windows/Temp/
 * %appdata%/Battle.net/ *(cf ci-dessous pour les chemins avec des %)*
 * %localappdata%/Battle.net/
 * %localappdata%/Blizzard Entertainment/

Ensuite réinstallez Battle.net puis vos jeux.

Source du fix : [[Solved] Fix Almost any Battle.net bugs with these 9 quick fixes!](https://us.battle.net/forums/en/bnet/topic/20752501530).

*Pour ouvrir les dossiers avec des pourcentages, copiez collez la ligne dans la barre en haut de l'explorateur de fichier Windows. Exemple :*

[![Ouvrir appadata dans un explorateur de fichier windows](/assets/images/posts/battlenet-explorer.png)](/assets/images/posts/battlenet-explorer.png)

## Diagnostique réseau

### La base : traceroute, ping, pathping

L'erreur laisse penser à un problème réseau. Le support Blizzard demande alors de réaliser un `pathping` (une commande Windows) sur l'ip `80.239.208.193`.

```raw
Détermination de l’itinéraire vers 80-239-208-193.customer.teliacarrier.com [80.239.208.193]
avec un maximum de 30 sauts :
  0  MASTERRACE.home [192.168.1.40]
  1  liveboxipv4.home [192.168.1.1]
  2     *        *        *
Traitement des statistiques pendant 25 secondes...
            Source vers ici  Ce nœud/lien
Saut RTT    Perdu/Envoyé = % Perdu/Envoyé = % Adresse
  0                                           MASTERRACE.home [192.168.1.40]
                                0/ 100 =  0%   |
  1    0ms     0/ 100 =  0%     0/ 100 =  0%  liveboxipv4.home [192.168.1.1]

Itinéraire déterminé.
```

Étonnemment cette commande semble échouer sur l'IP indiquée, ce qui laisse présager un problème réseau. Ceci dit, après quelques recherches, elle échoue sur toutes les IPs. La raison m'est encore inconnue.

Cependant les commandes `ping` et `traceroute` fonctionnent parfaitement bien :

```raw
Envoi d’une requête 'Ping'  80.239.208.193 avec 32 octets de données :
Réponse de 80.239.208.193 : octets=32 temps=22 ms TTL=112
Réponse de 80.239.208.193 : octets=32 temps=18 ms TTL=112
Réponse de 80.239.208.193 : octets=32 temps=18 ms TTL=112
Réponse de 80.239.208.193 : octets=32 temps=18 ms TTL=112

Statistiques Ping pour 80.239.208.193:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 18ms, Maximum = 22ms, Moyenne = 19ms
```

<pre style="white-space: pre">
Détermination de l’itinéraire vers 80-239-208-193.customer.teliacarrier.com [80.239.208.193]
avec un maximum de 30 sauts :

  1    <1 ms    <1 ms    <1 ms  liveboxipv4.home [192.168.1.1]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3     1 ms     1 ms    <1 ms  xxxx.Caen.francetelecom.net [x.x.x.x]
  4     6 ms     6 ms     6 ms  xxxx.Paris.francetelecom.net [x.x.x.x]
  5     6 ms     6 ms     6 ms  193.252.137.74
  6     *        *        *     Délai d’attente de la demande dépassé.
  7     7 ms     7 ms     7 ms  BLIZZARD-EN.ear2.Paris1.Level3.net [212.73.205.158]
  8     8 ms     7 ms     8 ms  37.244.9.48
  9     *        *        *     Délai d’attente de la demande dépassé.
 10    17 ms    17 ms    18 ms  37.244.10.101
 11     *        *        *     Délai d’attente de la demande dépassé.
 12     *        *        *     Délai d’attente de la demande dépassé.
 13    18 ms    17 ms    18 ms  80-239-208-193.customer.teliacarrier.com [80.239.208.193]

Itinéraire déterminé.
</pre>

Ces résultats suffisent à disqualifier un problème de réseau basique. On peut donc avancer dans nos recherches et regarder ce qui se passe sur Wireshark. 
### L'artillerie lourde : Wireshark

On va démarrer notre capture un peu avant l'appuie sur le bouton "Mise à jour" et l'arrêter juste après l'apparition du message d'erreur. On constate donc que les échanges sont réalisés sur le port `1119`, un des ports de Battle.net avec deux IP différentes : en `HTTP` avec `185.60.112.06` et dans un protole incconu avec `80.239.208.193`.

[![Capture Wireshark](/assets/images/posts/battlenet-wireshark.png)](/assets/images/posts/battlenet-wireshark.png)

Il y aurait beaucoup à redire sur le fonctionnement du client Battle.net mais rien qui nous intéresse pour résoudre notre problème.

À cette étape, à part tenter de déserialiser le protocole incconu, il ne nous reste plus grand chose à faire côté réseau.

## Diagnostique processus

Le bug étant propre à la machine, il est intéressant de voir comment le processus échange avec le reste du système. Je me suis donc mis en quête d'un équivalent de [`strace`](https://linux.die.net/man/1/strace) pour Windows. Suite aux conseils dans un fil de discussion sur [Stackoverflow](http://stackoverflow.com/questions/3847745/systrace-for-windows), je me suis tourné vers [Process Monitor](https://technet.microsoft.com/en-us/sysinternals/processmonitor.aspx)

Cette application permet entre autre de suivre les différents appels au registre, au système de fichier, au réseau et les différentes threads crées par l'application.

On commencer par cliquer sur "Filter", puis "Filter...". Là on va choisir dans le premier menu déroulant "Process Name", puis "contains" dans le second et ensuite on va taper "Battle". On clique sur ajouter pour rajouter le filtre.

[![Process Monitor Filter](/assets/images/posts/battlenet-processmonitor1.png)](/assets/images/posts/battlenet-processmonitor1.png)

Maintenant, vous ne devriez plus que voir les informations liés aux processus Battle.net. Les 3 icônes à droite de sauvegarde servent, dans l'ordre, à :

 - Lancer/Stopper la capture
 - Activer/Désativer l'autoscroll
 - Vider les logs

Et les 5 icônes les plus à droite servent à filtrer les évènements. Voici un exemple de ce que l'on peut obtenir :

[![Process Monitor Liste](/assets/images/posts/battlenet-processmonitor2.png)](/assets/images/posts/battlenet-processmonitor2.png)

Process Monitor m'a permis de me rendre compte de deux choses :

 * Un fichier de log était créé (et j'avais donc son chemin)
 * Battle.net essayait d'accéder à un dossier dans Battle.net.8423 qui n'existait pas.

J'ai alors essayé de supprimer le dossier Battle.net.8423. Mauvaise idée, ça plante complètement Battle.net. Lors de la réinstallation, j'obtiens une erreur de mise à jour du client Battle.net En lisant les logs, je trouve l'erreur "`Failed to fetch Build config`" que je cherche sur Internet et qui m'emmène vers le lien cité dans la partie "Solution". Il suffit donc de supprimer un certains nombre de fichiers. Probablement qu'une recherche plus poussée dans Process Monitor aurait pu nous ammener à la même conclusion.

Si nous n'avions pas trouvé de solution, il nous restait la possibilité d'utiliser un debugger / décompilateur (comme IDA ou radare), dans le respect des lois, pour comprendre ce qu'il se passe dans l'executable

## Conclusion

Finalement, l'erreur de Blizzard était trompeuse et la commande pour tester la connectivité réseau (`pathping`) induit elle aussi en erreur. De plus leur client semble avoir tendance à répliquer ses fichiers partout et à les corrompre par la même occasion. Enfin, le fil de discussion obscure mériterait d'être intégré à la FAQ qui est très laconique.
