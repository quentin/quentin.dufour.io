---
layout: post
slug: tout-sur-les-serveurs-teeworlds-partie-1
status: published
title: Créer un serveur Teeworlds
description: Un serveur Teeworlds basique
disqus: true
category: operation
tags:
- teeworlds
- jeu
- serveur
---

Teeworlds est un FPS 2D tout mignon, puisque vous incarnez une petite boule de couleur, ressemblant plus ou moins à un Kirby. Mais ne vous y méprenez, leurs intentions sont loins d'être pacifistes ! Vous pouvez télécharger le client et le serveur de ce jeu sur le site : <http://www.teeworlds.com>. En effet, ce dernier est totalement libre. Nous allons voir comment configurer notre serveur !

###Télécharger & installer

Le serveur et le jeu sont dans le même dossier zip que vous avez téléchargé. pas besoin d'installation ici, vous n'avez qu'à extraire votre zip.
Dedans vous trouverez un "teeworlds" et un "teeworlds_srv".

###Le fichier de configuration

Créer un fichier server.cfg dans votre dossier Teeworlds, puis éditez le avec un éditeur de texte (Bloc Note ou autre)
Voici un fichier de configuration basique :

	sv_name ServeurTeeworlds
	password motDePasse
	sv_rcon_password motDePasseAdmin
	sv_port 8303
	sv_gametype dm
	sv_warmup 10
	sv_map dm1
	sv_maprotation dm1 dm2 dm6 dm7 dm8 dm9
	sv_max_clients 8
	sv_scorelimit 20
	sv_tournament_mode 0
	sv_motd Bienvenue sur notre serveur !
	sv_powerups 1
	sv_timelimit 0
	sv_spectator_slots 0

Quelques expliations, sv_name correspond au nom de votre serveur. password est le mot de passe qui sera demandé aux gens qui se connectent. En laissant le champ vide, aucun mot de passe ne sera demandé. sv_rcon_password est un mot de passe pour administrer votre serveur. sv_gametype est le type de jeu : dm = deathmatch ; tdm = team deathmatch ; ctf = capture the flag.
sv_warmup est le temps d'échauffement en secondes, sv_map la map de départ et sv_maprotation les maps qui seront joués (pour voir une liste, allez dans le dossier data puis maps de votre dossier teeworlds. sv_max_clients est le nombre max de joueurs, sv_scorelimit la limite de score. sv_motd le message du jour.

###Le lancement

Ouvrez un terminal (touche windows + r puis cmd sous Windows) puis rendez vous dans le dossier :
(cd C:\Chemin\Vers\Teeworlds ou sous Linux cd /home/utilisateur/chemin/vers/Teeworlds).
Une fois dans le dossier lancez la commande
sous Linux :

	./teeworlds_srv -f ./server.cfg

ou sous Windows :

	./teeworlds_srv.exe -f ./server.cfg

Voilà, vous avez maintenant un serveur qui fonctionne, n'oubliez pas de rediriger le port 8083 de votre routeur vers votre serveur si vous voulez que des gens s'y connecte depuis internet et de couper vos pare-feux (ou encore mieux, de les configurer !)
