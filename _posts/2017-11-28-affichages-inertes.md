---
layout: post
slug: affichages-inertes
status: published
sitemap: true
title: Affichages inertes
description: Le rétro c'est chic
category: divers
tags:
- electronique
---

Ce que j'appelle affichage inerte, à défaut de connaître le vrai terme, correspond à des techniques d'affichage ne demandant pas ou peu d'électricité quand le contenu n'est pas mis à jour. Je me suis mis à la recherche de ces dernières car elles pourraient être très pratiques pour créer un affichage peu souvent mis à jour, comme pour de la surveillance (monitoring), pour des outils d'intégrations continus (comme Jenkins), des emails, des rappels, la météo, etc.

## Affichage à palettes

[![Affichage à Paris](/assets/images/posts/inerte_01.jpg)](/assets/images/posts/inerte_01.jpg)
*Gare du Nord - L.Willms - CC-BY-SA - [Wikipedia](https://commons.wikimedia.org/wiki/File:Gare_du_Nord_Fallblattanzeiger_Departure-board.JPG)*


Aussi appelés split flap, flap board ou encore Solari board, ces afficheurs étaient initialement construits par l'entreprise italienne, Solari Di Udine. Encore aujourd'hui, ils sont chers à l'achat. On en retrouve principalement dans les gares bien qu'ils aient tendance à disparaître. En effet, ils seraient chers à maintenir et renderaient la circulation difficile à cause de l'accumulation des voyageurs autour de ces panneaux.

Le cliquetis qu'ils émettent est souvent considéré comme un atout, car il permet de signifier une mise à jour de l'affichage. Aux Etats-Unis, ce dernier a été adapté pour [un affichage LED dans une gare de Boston](http://archive.boston.com/news/local/massachusetts/articles/2006/04/06/nostalgia_for_noise_at_south_station/).

On peut trouver plusieurs oeuvres d'art s'inspirant de ce concept, dont une à Stanford où, plutôt que de mettre des lettres sur les palettes, l'artiste les a peintes de différentes couleurs :

<iframe width="520" height="315" src="https://www.youtube.com/embed/ttajKgpJTns?rel=0&amp;start=10" frameborder="0" allowfullscreen></iframe>

<br/>
D'un point de vue commercial, ils sont encore produits mais seulement au cas par cas, et sont très probablement beaucoup plus couteux que des écrans traditionnels. Un designer anglais, [Tom Lynch](http://tomlynch.co.uk/split-flap-display/) et une entreprise, [OatFoundry](http://www.oatfoundry.com/split-flap/) se proposent d'en vendre :

<iframe width="520" height="315" src="https://www.youtube.com/embed/cfVviJXjN6w?rel=0" frameborder="0" allowfullscreen></iframe>

<br/>
Mais peut-on en fabriquer un soit-même ? Certains ont réussi. Mais pour commencer, comment ça marche ? Chaque palette possède sur sa face avant la partie haute d'un signe, et sur sa face arrière, la partie basse du signe suivant. Un élément retient la palette du haut pour que les deux palettes puissent être à la verticale et reconstituer le signe.

[![Brevet](/assets/images/posts/inerte_02.png)](/assets/images/posts/inerte_02.png)
*Brevet - E. Cappellari - [Wikipedia](https://commons.wikimedia.org/wiki/File:Analog_clock_with_digital_display.png)*

Certaines personnes ont posté des vidéo sur Youtube de systèmes qu'elles ont réussi à récupérer. Après une recherche rapide, je n'ai rien trouvé en seconde main. Par contre il existe plusieurs guides pour en construire un de zéro :

 * [Split Flap Display par Jonathan Odom](http://www.instructables.com/id/Split-Flap-Display/) proposant un kit complet avec une finition exemplaire
 * [splitflap par Scott Bezek](https://scottbez1.github.io/splitflap/) est un projet assez poussé, proposant des solutions pour construire l'objet avec peu de moyens.
 * [Dead simple split flap display par Piotr K.](https://www.thingiverse.com/thing:2369832) est un guide avec peu d'étapes et en impression 3D mais ne propose pas de boitier.
 * [Split Flap Display par mlo](https://hackaday.io/project/1281-split-flap-display) propose un afficheur en bois et avec des feuilles pour commencer, avec un journal de construction.

<br/>
Avec beaucoup de motivations, certains ont réussi à créer des choses impressionnantes :

<iframe src="https://player.vimeo.com/video/233180787" width="520" height="293" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<br/>
Si vous voulez en connaitre plus sur l'histoire de ces machines, je vous recommande le [Webdoc d'AriaDR](http://www.ariadr.fr/les-webdocs/splitflap/), réalisé dans le cadre d'un projet destiné à *recenser et étudier les innovations oubliées, abandonnées, délaissées ou résurgentes*. Ce dernier retrace l'histoire de ces panneaux de leur création à aujourd'hui.

## Girouette à pastilles

[![Affichage girouette à pastilles](/assets/images/posts/inerte_03.jpg)](/assets/images/posts/inerte_03.jpg)
*Bus au départ - ŠJů - CC-BY-SA - [Wikipedia](https://commons.wikimedia.org/wiki/File:D%C4%9B%C4%8D%C3%ADn,_autobusov%C3%A9_n%C3%A1dra%C5%BE%C3%AD,_displej_odjezd%C5%AF.jpg)*

On les retrouve aussi sous les noms Flip-dots display et Flip-disc display. Ces affichages se retrouvent beaucoup dans la signalétique des transports en commun, particulièrement les bus. Ils ont été développé par Ferranti-Packard, une filiale canadienne de Ferranti, à la demande d'Air Canada, mais utilisé la première fois par la bourse de Montréal en 1961.

D'un point de vue industriel, seule l'entreprise polonaise [AlfaZeta](https://flipdots.com/en/home/) semble produire encore ces dispositifs pour autre chose que l'évènementiel / l'aspect retro. L'entreprise [Breakfast NY](https://breakfastny.com/flip-disc/) (vidéo) ne précise pas si elle produit elle-même les panneaux ou si elle se base sur les panneaux d'AlfaZeta pour ses produits (capture d'image et raffraichissement à 30 images par seconde).

<iframe width="520" height="315" src="https://www.youtube.com/embed/s94PscZJ5EE?rel=0" frameborder="0" allowfullscreen></iframe>

<br/>
Pour ce qui est du fonctionnement, chaque pastille correspond à un pixel. D'un côté la pastille est noire, de l'autre elle est peinte d'une couleur flurorescente. La pastille possède un aimant, on obtient sa rotation en faisant passer de l'electricité dans un fil pour soit attirer l'aimant, soit le repousser.

[![Explication de la girouette à pastille](/assets/images/posts/inerte_04.png)](/assets/images/posts/inerte_04.png)
*Image extraite du site web d'AlfaZeta*

Pour ce qui est de fabriquer soit même son afficheur *girouette à pastilles*, cela semble plus compliqué, à ce jour je n'ai trouvé aucun guide pour en réaliser un de zéro.
En effet, l'élément de base semble être souvent l'afficheur [AlfaZeta Flip-Dot Boards XY5 7x28](https://flipdots.com/en/products-services/flip-dot-boards-xy5/). Malheureusement le prix n'est pas public.
Une autre possibilité est de récupérer des afficheurs de bus et de faire de la rétro ingénierie comme pour le projet [Dottie](http://dhenshaw.net/art/Dottie/start.html).

<iframe width="520" height="315" src="https://www.youtube.com/embed/68SsdXE5wMg?rel=0" frameborder="0" allowfullscreen></iframe>

<br/>
Bon courage si vous souhaitez vous lancer dans un tel projet donc !

## Afficheur 7 segments mécaniques

En anglais "mechanical seven-segment display", à ne pas confondre avec les autres affichages 7-segments qui nécessitent d'être alimentés en permanence. C'est probablement le dispositif le moins cher à produire de cet article. Par contre il ne permet d'afficher que des nombres.

On peut trouver plusieurs guides pour les fabriquer soit même, avec des briques Lego, ou des pièces en pastiques imprimées. L'idée est d'avoir, sur un fond noir, des segments fluorescents que l'on tourne de 90°C lorsqu'on ne veut pas les afficher.

<iframe width="520" height="315" src="https://www.youtube.com/embed/EEdd_M6f8BU?rel=0&amp;start=10" frameborder="0" allowfullscreen></iframe>
Un exemple de réalisation DIY : [Servo Driven 7-segment Display](http://g33k.blogspot.fr/2013/12/servo-driven-7-segment-display.html)

Ceci dit, on peut trouver ces pièces pour environ 10$. Il est intéressant de noter que AlfaZeta vend également des afficheurs 7-segments mécaniques de différentes tailles. Par contre, je n'ai trouvé aucun afficheur 14-segments mécaniques. Il n'est donc pas possible d'afficher des lettres.

![AlfaZeta](/assets/images/posts/inerte_05.jpg)
*Un grand affichage 7-segments AlfaZeta*

<!--
## Encre électronique

*Bientôt*

## Prototypes

*Bientôt : [Pixel Track by Cloudberg](https://www.designboom.com/technology/pixel-track-berg-cloud-connected-signage-system-05-29-2014/)*

-->
