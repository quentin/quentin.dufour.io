---
layout: post
slug: manette-xbox-reparation
status: published
sitemap: true
title: Réparation d'une manette Xbox One
description: La panne la plus bête du monde
category: divers
tags:
---

Depuis maintenant plusieurs mois, j'ai perdu la gachette gauche de ma manette Xbox One, ma seule manette de jeu.
Ce qui est plutôt embêtant, car elle me rend de fiers services quand je joue à Trackmania et plus récemment à Firewatch.
Et pas de chance, bien que la peur n'évite pas le danger, le frein sur la gachette gauche évite les murs sur Trackmania.

[![Panneau Trackmania "La peur n'évite pas le danger"](/assets/images/posts/xbox-controller-tm.jpg)](/assets/images/posts/xbox-controller-tm.jpg)

Pas mieux du côté de Firewatch, car il s'agit du talkie walkie, un élément central du jeu. Sans grand espoir, je me suis donc lancé dans la réparation de cette manette, qui n'en est pas à sa première défaillance. En effet, j'ai dû jeter le cable micro-usb à usb fourni avec car il ne fonctionnait plus.

## Dissection de la manette

iFixit, comme toujours, propose [un guide](https://fr.ifixit.com/Vue+%C3%89clat%C3%A9e/Xbox+One+Wireless+Controller+Teardown/72986) très pratique pour démonter la manette, que j'ai suivi scrupuleusement.

En démontant la manette, je me suis rendu compte qu'il manquait une vis à un endroit. Simple oubli ou début de réponse ?

[![Vis manquante](/assets/images/posts/xbox-controller-td1.jpg)](/assets/images/posts/xbox-controller-td1.jpg)

Une fois le démontage fini, je me retrouve avec les différents circuits de ma manette.

[![Circuit électronique manette xbox](/assets/images/posts/xbox-controller-td2.jpg)](/assets/images/posts/xbox-controller-td2.jpg)

En regardant un peu mieux, je me rends compte qu'une vis est aimantée sur la gachette.
En effet, le bout de la gachette possède un aimant relativement puissant.
Même en agitant la manette dans tous les sens, la vis ne risquait pas de se désaimanter.
Vis qui provient probablement de notre emplacement vide précédent...

[![La vis perdue est aimantée sur une gachette !](/assets/images/posts/xbox-controller-td3.jpg)](/assets/images/posts/xbox-controller-td3.jpg)

Une fois enlevée, un rapide test permet de vérifier que ma gachette gauche fonctionne de nouveau

## Conclusion

En remontant les vis, je me suis rendu compte qu'une des vis tournait plus ou moins dans le vide.
Je pense que le taraudage d'une pièce doit être cassé. La vis avait l'air de bien tenir pour l'instant, je l'ai donc laissée.
Mais si elle venait à se promener de nouveau, un peu de colle pourrait résoudre le problème. Ou simplement l'enlever...
Bref, une réparation plus simple que prévue, c'est suffisament rare pour être signalé ! Essayez de réparer vos objets,
parfois ça vaut le coup !
