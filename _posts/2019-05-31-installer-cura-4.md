---
layout: post
slug: alfawise-u30-et-fedora-linux
status: published
sitemap: true
title: Utiliser une Alfawise U30 depuis Fedora
description: Pour des impressions 3D libres
category: operation
tags:
---

L'Alfawise U30 n'a été intégrée que récemment dans Cura et n'est disponible que depuis le dépôt.
Qui plus est, la dernière version majeur de Cura, la version 4, est encore en beta et n'est pas non plus disponible dans Fedora stable.

Nous allons donc installer Cura 4 beta depuis Fedora Rawhide (unstable) puis télécharger les fichiers de définition de l'Alfawise U30 depuis le dépôt officiel.
Une fois le code actuel du dépot publié dans une version stable, ce guide sera inutile.

## Installer Cura 4

Nous allons tout d'abord commencer par installer un paquet qui permet d'activer les dépôts instables (mais seulement sur demande) :

```bash
sudo dnf install fedora-repos-rawhide
```

Ensuite installons Cura depuis les dépôts instables.

```bash
sudo dnf install --nogpg --enablerepo=rawhide cura
```

**Il semble y avoir un bug avec les clés PGP qui ne sont pas correctement installés pour le dépôt instable, j'ai donc désactivé la vérification juste pour cette commande.
Cela reste une très mauvaise idée et corriger durablement ce bug en important les clés PGP serait une meilleur solution.**

## Installer les définitions de l'Alfawise U30

Les configurations pour les imprimantes 3D sont contenues dans des fichiers JSON.
Nous allons donc les télécharger au bon endroit, tout simplement :

```bash
cd /usr/share/cura/resources/definitions/
sudo wget https://raw.githubusercontent.com/Ultimaker/Cura/137619567a1d68139444f0fea76a022d63d86a0b/resources/definitions/alfawise_u30.def.json
cd /usr/share/cura/resources/extruders
sudo wget https://raw.githubusercontent.com/Ultimaker/Cura/057c30f86e86635721e8a269d864e1597699c834/resources/extruders/alfawise_u30_extruder_0.def.json
```

Et voilà, ce devrait être bon, bonne impression !
