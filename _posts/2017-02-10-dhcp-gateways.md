---
layout: post
slug: passerelles-et-dhcp
status: published
title: DHCP et la gestion des passerelles
description: DHCP DISCOVER
category: operation
tags:
---

Après une récente modification de mon serveur DHCP, je me retrouvais dans l'impossibilité de connecter une tablette à mon réseau.
Cette dernière restait bloquée à `Récupération de l'adresse IP`. Après avoir un peu cherché sur internet, je n'ai pas trouvé de solution satisfaisante.
Le serveur DHCP fonctionnait avec les autres ordinateurs.

## Le protocole DHCP

Le schéma propose une version simplifiée des échanges entre un client et un serveur DHCP.

```raw
Client            Serveur
  |---- DISCOVER --->|
  |<----- OFFER -----|
  |---- REQUEST ---->|
  |<------ ACK ------|
```

*Il est intéressant de noter que BOOTP est l'ancêtre de DHCP et que DHCP est rétro compatible avec BOOTP. De ce fait, la plus part du paquet DHCP va être vide, et les paramètres DHCP vont se trouver dans les options du paquet BOOTP.*

## Logs

J'ai commencé par regarder les logs de `isc-dhcp-server` :

```raw
dhcpd[2207]: DHCPREQUEST for 192.168.1.77 (192.168.1.254) from aa:aa:aa:aa:aa:aa (android-xx) via br0
dhcpd[2207]: DHCPOFFER on 192.168.1.77 to aa:aa:aa:aa:aa:aa (android-xx) via br0
dhcpd[2207]: DHCPREQUEST for 192.168.1.77 (192.168.1.254) from aa:aa:aa:aa:aa:aa (android-xx) via br0
dhcpd[2207]: DHCPOFFER on 192.168.1.77 to aa:aa:aa:aa:aa:aa (android-xx) via br0
```

J'ai des doutes sur la terminologie utilisée par le serveur DHCP vis à vis du nom des messages envoyés sur le réseau.
D'autant que Bootp utilise aussi les termes Request et Reply pour des cas différents :

 * Une Request Bootp peut-être un message Discover DHCP
 * Une Reply Bootp peut-être un message Offer DHCP

Il semble ici que le terme DHCPREQUEST fasse référence à un message DISCOVER DHCP.
Je suppose donc que la tablette envoie des requêtes DISCOVER et que le serveur lui envoie un paquet OFFER auquel la tablette ne répond pas.
Si on reprends le schéma ci-dessus, je suppose que l'on a ces échanges :

```raw
Client            Serveur
  |---- DISCOVER --->|
  |<----- OFFER -----|
  |                  |
  |---- DISCOVER --->|
  |<----- OFFER -----|
```

## Modification de la configuration en aveugle

J'ai donc modifié la configuration de mon serveur DHCP en la simplifiant le plus possible, jusqu'à ce que ma tablette réussisse à se connecter.
Le coupable était la ligne :

```raw
option routers 192.168.1.254, 192.168.1.1;
```

En ne spécifiant qu'une seule passerelle, le problème est résolu :

```raw
option routers 192.168.1.254;
```

Mais d'où vient ce problème ?

## Traffic réseau

Je me suis demandé ce que pouvait bien envoyer `isc-dhcp-server` à ma tablette pour qu'elle n'en veuille pas.
J'ai donc comparé les paquets envoyé avec les deux configurations différentes.

### Outils

Pour écouter le traffic réseau j'ai utilisé `tcpdump`. L'option `-vv` est bien pratique car elle permet d'expliquer le contenu du paquet.
Dans le cas d'un protocole inconnu, `-XX` permet d'avoir un hexdump. Les deux sont combinables.

```raw
sudo tcpdump -i br0 -vv -e -n port 67 or port 68 and ether host aa:aa:aa:aa:aa:aa
```

### Messages envoyés

On lance une capture dans le mode avec les deux gateways. *Pour des questions de lisibilité, on filtre sur le nom du paquet DHCP avec grep*.

```raw
$ sudo tcpdump -i br0 -v -e -n port 67 or port 68 and ether host 00:08:22:12:af:fd|grep DHCP-Message
tcpdump: listening on br0, link-type EN10MB (Ethernet), capture size 262144 bytes
	    DHCP-Message Option 53, length 1: Discover
	    DHCP-Message Option 53, length 1: Offer
	    DHCP-Message Option 53, length 1: Discover
	    DHCP-Message Option 53, length 1: Offer
	    DHCP-Message Option 53, length 1: Discover
^C8 packets captured
8 packets received by filter
0 packets dropped by kernel
```

On observe en boucle un message `DISCOVER` suivi d'un message `OFFER` et jamais de paquet `REQUEST`.
Ce qui confirme bien ma thèse suite à la lecture des logs.

### Comparaison des deux paquets OFFER

Après comparaison du dump hexadecimal des deux paquets `OFFER`, ces derniers sont quasiment identiques. Les deux seules différences sont :

  * Celui avec les deux gateways fait 4 octets de plus. On notera que c'est aussi la taille d'une ipv4, et c'est du à la seconde adresse.
  * Des valeurs générés aléatoirements sont aussi différentes.

En tout cas, `tcpdump` n'a pas de problème à décoder nos champs `gateway` dans les deux cas.

```raw
Default-Gateway Option 3, length 4: 192.168.1.254
```
```raw
Default-Gateway Option 3, length 8: 192.168.1.254,192.168.1.1
```

A la suite de ça, je suppose donc que c'est le client DHCP qui ne sait pas quoi faire de ces deux gateways.

## Etude du client DHCP

Le protocole DHCP indique que le client doit envoyer son nom :

```raw
Vendor-Class Option 60, length 16: "android-dhcp-6.0"
```

Je connais maintenant le nom du client DHCP utilisé par la tablette.
Mais comment débugger ça du côté d'Android ? Retouver le code source ? Regarder les logs ? Mais où et comment faire quand on est pas root ?

*A suivre*
