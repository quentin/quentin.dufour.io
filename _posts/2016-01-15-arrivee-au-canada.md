---
layout: post
slug: arrivee-au-canada
status: published
title: Arrivée au Canada
description: Et départ de France
category: divers
tags:
---

Mais comment me-suis je retrouvé au milieu de Montréal un 14 janvier 2016 ? Pas totalement par hasard en tout cas.

J'étudie actuellement à l'[INSA Rennes](http://insa-rennes.fr), et dans le cadre de mon cursus, je dois passer un certains temps à l'étranger. J'ai donc choisi l'École Polytechnique de Montréal, comme Loïc, qui est dans ma promo et sera mon colocataire au Canada. Et me voilà donc au milieu de la belle province. Mais encore faut il s'y rendre ! Et la journée du jeudi 14 janvier a été (très) longue.

## 5h00 : Le réveil

C'est le moment de rassembler ses dernières affaires avant de partir, vérifier qu'on a rien oublié une fois, deux fois et oublier des choses. Direction le métro de Rennes pour se rendre à la gare.

## 5h45 : La gare

Nous arrivons à la gare de Rennes sans encombre. Hasard s'il en est, nous retrouvons Flavien, un autre camarade promo qui part pour Sherbrooke. Nous discutons un peu, il prend le train suivant qui arrive directement à l'aéroport. Loïc et moi devrons par contre traverser Paris en transport en commun. On ne peut pas gagner à tous les coups. Notre train part à 6h05, tout va bien.

## 8h12 : Paris

A notre arrivée à Paris, nous nous dirigeons vers le métro qui se trouve dans le sous-sol de Montparnasse - nous avions soigneusement étudié notre plan avant le départ. Ligne 6, arrêt Denfert Rochereau pour prendre le RER B. Malgrès l'heure, nous ne rencontrons pas de difficulté particulière avec nos valises.

## 9h30 : Roissy Charles de Gaulle

L'aéroport est immense et impressionnant. Ce n'est pas Loïc qui dira le contraire. Nous enregistrons nos bagages et récupérons notre carte d'embarquement. J'hérite alors d'une gomette rouge sur mon passeport alors que Loïc et Flavien, qui vient de nous rejoindre, ont eu une gomette verte. Un mystère de plus.

Pensant avoir quelques minutes de libre, nous nous posons sur des sièges de l'aeroport. Erreur ! Il faut encore passer la douane et le contrôle de sécurité. C'est reparti. Le temps de montrer patte blanche, nous voilà de l'autre côté.

Nous nous retrouvons alors entre des boutiques Dior, Rolex et autres marques prestigieuses. Bon, et bien ce n'est pas avec ça que l'on va manger ! Nous trouvons un peu plus loin un bon vieux Mac Donalds. Nous en profitons aussi pour admirer les avions et la piste.

![Vue des pistes depuis le terminal](/assets/images/posts/arrivee-canada-1.jpg)

## 12h10 : L'embarquement

L'embarquement commence à 12h10, mais étant donné le nombre de personnes sur ce vol, ce n'est pas trop tôt. Ce dernier se fait par zone, chaque zone étant appelée à son tour. Quelques minutes plus tard, nous voilà dans la passerelle. Un dernier employé d'Air Canada vérifie notre passeport, et nous pose cette question qui semble assez célèbre : "Vous allez où ?". Je ne crois pas que "dans l'avion" ou toute autre blague du même accabit soit vraiment bien accueillie. Je vous laisse le soin d'essayer.

## 13h05 : Le vol

Nous voilà dans un magnifique Boeing 777 série 3000. Un gros avion en somme. Avec tout le confort qui se doit, et donc un écran par passager, tactile, avec des films, des séries, et même des courts métrages. Evidémment, rien n'est parfait, et les accoudoirs sont rendus inutilisables par les commandes qui sont reportées dessus, et particulièrement la lumière qui a la facheuse habitude de s'allumer dès que l'on pose son coude sur l'accoudoir.

Pendant ces 7 heures de vol, pas le temps de s'ennuyer, le personnel d'Air Canada se relaie en continu pour distribuer à manger et à boire. Et ça, c'était bien !

L'avion reste tout de même assez bruyant, et c'est donc dur de s'endormir pendant le voyage. Notons aussi la traditionnelle carte de déclaration à remplir. Notes pour les français, É-U est l'abbréviations pour États-Unis et pas European Union, comme beaucoup le pense. Tout d'abord car il y a un accent sur le É, ensuite parce que si vous retournez votre feuille, vous verrez qu'il est inscrit U.S. Une petite pensée pour tout ceux qui sont restées à la frontière à cause de ça.

## 14h40 : Pierre-Elliott-Trudeau

Nous venons de passer 7 heures dans l'avion mais avec le décallage horaire, il n'est que 14h40 à Montréal. Nous devons maintenant faire la queue pour passer la frontière. C'est à ce moment que nous nous rendons compte que le groupe "Salut c'est cool" voyageait aussi dans notre avion, étant un peu devant nous dans la queue.

Une fois la frontière passée, nous pouvons alors récupérer nos bagages. A la sortie, nous remettons notre carte de déclaration, prélablement tamponnée lors du passage de la frontière. Nous voilà au Canada !

## 15h20 : Direction notre logement

Nous avons réservé un logement sur Internet, via une entreprise qui s'est spécialisée dans la location aux étudiants en échange : [Irie Location](http://irielocation.com/). Encore faut-il s'y rendre. Situé à 15 minutes de Polytechnique Montréal, il faut par contre au moins 1 heure pour s'y rendre depuis l'aéroport qui se trouve assez excentré par rapport au centre de Montréal.

Heureusement, un bus express relie l'aéroport et le centre ville de Montréal. Nous nous installons à bord. Il faut compter un peu de temps tout de même, car le bus est souvent pris dans les bouchons. Ce dernier nous laisse près d'une station de métro. Après Paris, le métro Montrealais est bien vide et facile à prendre !

## 16h20 : Le début du doute

Arrivé à notre station, nous nous retrouvons dehors, sans savoir dans quelle direction aller ni où s'arrêter et c'est là que...

 * Que mon téléphone n'a pas de réseau, car chez Free Mobile, si vous activez le forfait bloqué, ce dernier ne se connecte pas en itinérance. Il faut désactiver cette fonctionnalité dans l'espace client (2 fois même dans certains cas).
 * Que les arrêts de bus n'ont pas de carte, ni d'horaires.
 * Qu'à l'intérieur du bus, il n'y a pas non plus de plan.
 * Qu'il fait froid et que la nuit commence à tomber.

Nous décidons finalement de rejoindre notre logement à pied. "Ça ne doit pas être si loin". En fait si, quand même. Nous demandons notre chemin aux gens que nous croisons. Finalement, nous finissons par atteindre notre but sans trop d'erreur. Une personne de l'agence nous attendait déjà. Elle nous donne les clés, et nous voilà à la maison !

## 16h45 : Home sweet Home

Il est bientôt 17h00, nous avons donc voyagé pendant 18h. Nous en profitons pour nous poser un peu. Nous ressortons pour trouver à manger (dans un subway, gastronomie oblige), et rentrons vite nous coucher.

En conclusion, prévoyez bien de partir reposé. Préparez votre voyage au maximum si vous comptez le réaliser d'une traite. C'est vraiment exténuant !
