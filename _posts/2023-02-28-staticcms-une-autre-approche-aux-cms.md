---
layout: post
title: StaticCMS, une autre approche aux CMS
date: 2023-02-28T14:11:46.488+01:00
status: published
sitemap: true
category: developpement
description: Si Worpdress tient le haut du panier des CMS, il n'est pas dénué de
  défauts. Découvrez Static CMS, une alternative qui permet de concevoir des
  sites webs sobres, rapides, et très configurables
---
Deuxfleurs a fait le choix de stocker ses données via un système de son cru nommé [Garage](https://garagehq.deuxfleurs.fr). Ce dernier a de nombreux avantages dont un va particulièrement nous intéresser ici, c'est de publier un “dossier” (on appelle ça un _bucket_) directement comme un site web. C'est très efficace car on minimise les abstractions et les calculs à réaliser : ça consomme peu de ressources chez Deuxfleurs, et ça fait des sites rapides à consulter pour les internautes. Par contre, le site web doit être sous forme de fichiers HTML, CSS et Javascript : on ne peut pas le générer à la volée comme le fait un Wordpress, en allant chercher les informations dans une base de données. On appelle ce genre de site, “des sites statiques”, car ce sont simplement les mêmes fichiers qui sont envoyés à tout le monde pour constituer le site final.

Alors si vous avez l'habitude de réaliser vos sites webs à la main, vous devriez être aux anges : en une commande, ou un glisser/déposer, votre site web est en ligne. Mais si vous êtes plutôt du genre à utiliser une interface, comme un système de gestion de contenu, alors tout devient plus compliqué…

À moins que… à moins qu'on puisse combiner “sites statiques” et “CMS”. Et heureusement pour nous, les sites statiques sont à la mode, donc pas mal de gens ont travaillé sur la question ces dernières années ! Quand on est cool, on appelle ça une [Jamstack](https://jamstack.org/). Ici rien à voir avec la confiture ou des concerts de musique, JAM veut dire Javascript, API et Markup. Ce concept est issu de réflexions par des développeurs web sur l'architecture idéale pour développer des sites webs. Nous, on va se contenter de garder les principes qui nous intéresse là dedans.

![](/assets/screenshot-2023-02-28-at-14-47-22-for-fast-and-secure-sites-jamstack.png)

Un premier principe, c'est un découplage entre le _front_, c'est à dire le site web que voient les internautes, et le _back_, c'est à dire l'interface qui permet de créer, modifier, ou supprimer du contenu. Un autre principe, c'est que le _front_ qui est présenté aux internautes doit être le plus _pré-calculé_ possible pour que ce soit aussi rapide et économe que possible de charger une page web. À partir de ces deux principes, on a tout un ensemble de conséquences très désirables (bonnes performances, faibles coûts, sécurité, passage à l'échelle, mise en cache facilitée, etc.).

Maintenant qu'on a vu la théorie, passons à la pratique, et prenons ce (billet de) blog comme exemple ! Vous lisez ce billet depuis le _front_ qui est en réalité une série de fichiers HTML, CSS et Javascript. Ces fichiers sont stockés dans Garage et ont été créés par un _générateur de site statique_, ici le vénérable [Jekyll](https://jekyllrb.com/), qui va transformer des fichiers [Markdown](https://fr.wikipedia.org/wiki/Markdown) en HTML. Bien sûr, Jekyll ne fonctionne pas tout seul, ni automatiquement, il faut le lancer à chaque modification. Pour automatiser ce lancement et l'envoi des fichiers, on va utiliser un _exécuteur de tâches_, ici [Drone](https://www.drone.io/). Maintenant, il faut bien stocker ces fichiers Markdown quelque part, pour ça on utilise Git, et plus particulièrement [Gitea](https://gitea.io). Enfin, parce qu'éditer ces fichiers Markdown à la main, et les envoyer dans Git, est une tâche fastidieuse, on a créer une interface _back_ qui permet de cacher toute cette complexité et proposer un environnement d'écriture, nous y voilà, je parle bien de [StaticCMS](https://www.staticcms.org/). Lors de votre travail de publication, vous n'aurez pas besoin de vous soucier de tout ces composants. Comme avec Wordpress, vous allez simplement intéragir avec une interface web de gestion de contenu.

Pour vous en convaincre, voici le tableau de bord de notre CMS :

![](/assets/20230228_15h03m03s_grim.png)

Et voici son interface d'écriture :





![](/assets/20230228_15h02m40s_grim.png)
À l'instant où je vais appuyer sur “Publier”, alors c'est tout le système qui va se mettre en branle automatiquement : mes modifications seront enregistrées (avec la gestion d'un historique pour pouvoir revenir en arrière), Drone va se lancer et construire les nouvelles pages de mon site web avec Jekyll, et ensuite les envoyer sur Garage.

Alors face à tout ces composants, on pourrait critiquer que ça ajoute une complexité importante, avec de nombreuses _pièces mobiles_ (_moving parts_ en anglais), c'est à dire des composants indépendants, qu'il faut configurer pour faire fonctionner ensemble, et qui ensuite ont leur propre cycle de vie. Tout d'abord, ces composants ont des interfaces bien définies et relativement stables dans le temps, ensuite, ces configurations peuvent être automatisées et cachées. _In fine_, une plateforme bien intégrée peut très largement réduire la complexitée perçue, tout en laissant une flexibilité très importante pour quelqu'un qui aurait des besoins spécifiques.

Concrètement, aujourd'hui, vous pourriez vous demander quelles sont les étapes pour publier votre site statique avec un CMS sur Deuxfleurs ?

Tout d'abord, il faut préparer un dépôt git pour votre site web. Il vous faut commencer par choisir un générateur de site statique (Jekyll, Hugo, Zola, etc.). Souvent ces générateurs ont une commande pour initialiser le dépôt avec un squelette de fichiers (exemple : `jekyll new mon-blog`)

Ensuite, suivez [le guide de StaticCMS](https://www.staticcms.org/docs/add-to-your-site-cdn) pour intégrer le CMS à votre dépôt de site statique. Vous devriez avoir à la fin un dossier admin avec 3 fichiers : `config.yml`, `index.html` et `static-cms-app.js` (je vous conseille en effet de “vendorer” le fichier plutôt que d'utiliser un CDN). Notez que les informations de la section _backend_ sont spécifiques à votre hébergeur, pour Deuxfleurs voici les paramètres à configurer :

```yml
backend:
  name: gitea
  repo: <username>/<repo>
  base_url: https://teabag.deuxfleurs.fr
  api_root: https://git.deuxfleurs.fr/api/v1
  branch: main
```

Vous devriez maintenant pouvoir valider vos modifications dans git, les pousser sur le dépôt, et lancer un serveur de développement local (exemple : `jekyll serve`). En accédant à [http://localhost:4000/admin/](http://localhost:4000/admin/) (pensez à adapter le port) vous devriez pouvoir accéder à votre interface CMS.

Une fois le CMS en place, vous pouvez configurer l'automatisation via Drone en ajoutant un fichier `.drone.yml`, puis en activant la CI sur votre dépôt, après avoir configuré les secrets pour publier sur Garage. Au final, vous pourrez ensuite travailler sur votre site web depuis l'URL complète de votre site, dans mon cas [https://quentin.dufour.io/admin/](https://quentin.dufour.io/admin/).

Bien entendu, nous pourrions imaginer une interface qui vous permettrait de choisir _un thème_ de site web, et qui vous créerait automatiquement le dépôt, avec les bons fichiers, et la configuration Drone qui va bien, de sorte qu'en moins de 5 minutes vous aillez un squelette en ligne sur lequel vous pouvez ajouter du contenu ! C'est pour une autre fois ça :-)
