---
layout: post
slug: premiers-jours-a-montreal
status: published
title: Premiers jours à Montréal
description: Avec un soupçon de jet lag
category: divers
tags:
---

En quelques jours à Montréal nous avons vu et fait beaucoup de choses. Dépaysement garanti à 100%. Et cette fois-ci, j'ai pris des photos.

## Découverte du quartier

Nous nous trouvons du côté de Côte des Neiges, derrière le Mont Royal. C'est un quartier sous la neige que nous avons découvert (il porte bien son nom, je sais). La vue au petit matin est jolie (pas un nuage). De quoi se motiver pour aller à Polytechnique pour s'inscrire.

![Notre rue](/assets/images/posts/premier-jour-1.jpg)

Nous avons décidé de braver le froid canadien et de nous y rendre à pied. Le campus universitaire de Montréal regroupe notre école, HEC ainsi que l'université. C'est assez impressionant. Et petite subtilité, beaucoup de batiments communiquent entre eux par des tunnels, de sorte qu'une fois à l'intérieur, vous n'avez plus besoin de sortir.

![L'université de Montréal](/assets/images/posts/premier-jour-2.jpg)

## Excursion au Mont Royal

Polytechnique Montréal se trouvant juste à côté du Mont Royal, nous avons décidé de nous y rendre à pied. Bon, en fait juste à côté, c'est vite assez loin au Canada. Le temps d'atteindre le Pavillon du lac aux Castors, il faisait nuit. Le lieu est par contre sublime sous la neige. Et il y a une patinoire !

En arrivant, nous sommes accueillis par ce traineau illuminé.

![](/assets/images/posts/premier-jour-3.jpg)

Avant de monter sur la glace, une photo s'impose. On remarquera que même si Loïc a l'air petit à côté du soldat, ce n'est pas totalement de sa faute, car il n'a pas de tambour comme ce dernier pour paraitre plus grand.

![](/assets/images/posts/premier-jour-4.jpg)

Les arbres illuminés se trouvent en réalité au milieu de la patinoire, dont nous pouvons faire le tour. Parfait pour une soirée romantique.

![](/assets/images/posts/premier-jour-6.jpg)

Après ça, nous avons décidé d'avancer un peu plus sur le Mont Royal, mais impossible de trouver un chemin qui nous emmenait à un point de vue. Et avec la nuit qui tombait, nous avons préféré redescendre sur Montréal. Plutôt que de mourir gelés dans le froid Canadien.

## Tim Hortons

Après avoir essuyé des bourrasques de vent bien fraiches, nous avons décidé de nous arrêter dans un Tim Hortons. En effet, il y a quelques années de ça, mon correspondant Canadien en avait fait l'éloge. La chaine a été fondée par un joueur de hockey canadien, et est actuellement la plus populaire de son genre au Canada (devant Mac Donalds et Subway). Ils servent principalement du café et des biscuits/donuts, mais aussi des sandwichs. Bref, tout est sur [Wikipedia](https://fr.wikipedia.org/wiki/Tim_Hortons).

Quelle fut ma surprise quand le serveur nous a dit qu'il ne parlait pas français, alors que tout était écrit en français. Apparemment, tout le monde parle anglais ici. Je ne sais pas encore si c'est dû au quartier, ou à la chaine.

Le passage de commande est particulier aussi, et nous ne savions pas qu'il fallait avancer au comptoir suivant pour récupérer sa commande. Une personne nous a remarqué, nous a indiqué où nous placer et nous a gentiment dit : "Vous venez d'arriver ? Bienvenue au Canada !". Comme quoi, les Canadiens n'ont pas volé leur réputation !

![](/assets/images/posts/premier-jour-7.jpg)

## Le centre de Montréal

Quelques jours plus tard, nous avons décidé de repartir en visite dans Montréal. Nous nous sommes arrêtés sur la place du Champs de Mars. Pas de Tour Eiffel en vue pourtant... Par contre le vieux palais de justice de Montréal et l'hôtel de ville en fond.

![](/assets/images/posts/premier-jour-10.jpg)

Nous avons fait escale au centre Eaton qui est un centre commercial géant, avec des restaurants et fast foods au rez de chaussée, et des boutiques de vêtements, de cosmétiques, de téléphones et autres sur les différents étages... Pratique quand on est parti au Canada sans chaussure, sans gant, sans écharpe et sans bonnet. Le principal intéressé se reconnaitra !

Chose amusante, il est possible d'entrer dans le centre depuis l'arrêt de métro sans avoir à sortir dehors.

Pour plusieurs raisons, les forfaits téléphoniques sont chers au Canada. Surtout quand on vient de France. A titre d'exemple, un forfait type 2h appels et sms illimités coûte plus de $25 (16€), comparé aux 2€ de Free Mobile en France. C'est toujours bon à savoir. A noter qu'il y a quelques années, les prix pratiqués en France étaient assez semblables...

![](/assets/images/posts/premier-jour-11.jpg)

En reprenant la route, nous avons croisé un batiment avec le logo des JO et cette imposante oeuvre devant. Je suppose que cet immeuble doit appartenir au Comite International Olympique (CIO). Encore un mystère !

![](/assets/images/posts/premier-jour-12.jpg)

Montréal regorge aussi de monuments, comme la basilique Notre Dame que nous avons prise en photo de nuit lors de notre première excursion.

![](/assets/images/posts/premier-jour-8.jpg)

## Le quartier chinois

Le quartier chinois est situé à côté de l'hotel de ville. Quartier bien à part, puisqu'il est totalement piéton. On y retrouve tout un tas de spécialités asiatiques plus surprenantes les unes que les autres. C'est décidé, on ira manger là-bas un de ces jours !

![](/assets/images/posts/premier-jour-13.jpg)

![](/assets/images/posts/premier-jour-14.jpg)


## Le vieux port

En continuant à l'est, nous arrivons au vieux port, sur les rives du Saint Laurent. Aujourd'hui, il ne semble plus y avoir beaucoup de bateaux, surtout en hiver où la glace a recouvert une bonne partie de ce dernier. On y retrouve le musée des sciences, ou encore le festival de musique Igloofest.

![](/assets/images/posts/premier-jour-15.jpg)

Tous les hivers, la ville de Montréal installe une patinoire. C'est même une institution, puisqu'elle était déjà là il y a 20 ans. Chaque jour, le thème des musiques passées est différent. Ce jour là, c'était les chansons francophones. Malheureusement, pas de Céline Dion à notre passage (cliché, toujours...). Ici tout le monde patine, même avec des poussettes pour certains ! Il faut dire aussi que le Hockey est le sport national.

![](/assets/images/posts/premier-jour-16.jpg)

En dépassant la patinoire, et en longeant le bord du Saint Laurent, nous arrivons jusqu'à la tour de l'horloge. L'été, se trouve aussi une plage artificielle pour profiter du soleil, sur le bord du port de plaisance.

![](/assets/images/posts/premier-jour-17.jpg)

Au-delà de la tour, la zone devient plus industrielle, et mes connaissances maritimes étant plutôt limitées, nous avons pu observer ce que j'appellerai des "gros bateaux".

![](/assets/images/posts/premier-jour-18.jpg)

Toujours lors de notre première excursion de nuit, nous avions profité quelques minutes du belvédère du musée des sciences pour prendre un panorama du vieux port, avant de redescendre rapidement car la nuit la température descend très vite, et nous étions très exposé au vent. En d'autres termes, j'avais les doigts gelés.

![](/assets/images/posts/premier-jour-9.jpg)

Bref, Montréal sous la neige offre de beaux paysages, mais aussi de surprenants endroits. Nous comptons encore explorer l'île Saint Hélène, ou encore le Mont Royal de jour, ainsi que le biodôme, et bien d'autres endroits.
