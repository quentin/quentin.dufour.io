---
title: About
permalink: en/about/
profile: true
---

Hello, dear visitor,

I put online this website in 2012, when I was still in high school, to share my discovery about computers.
Since then, computers are now my work and I still enjoy discovering and sharing knowledge about them.
In the following, you should get a quick overview of what get me busy outside of my blog posts.

## Now Now Now

I am currently improving [Deuxfleurs](https://deuxfleurs.fr)'s code and infrastructure, 
our non-profit experimental hosting service where 
we think how to make computers and networks more convivial, fair and emancipating.
In parallel, I am seeking a job in accordance with the ethic we follow at Deuxfleurs.

Link: [What is a Now section?](https://nownownow.com/about)

## Contributions to free software

At [Deuxfleurs](https://deuxfleurs.fr),
we develop our own distributed software suite: 
[Garage](https://git.deuxfleurs.fr/Deuxfleurs/garage/) to store data, 
[Diplonat](https://git.deuxfleurs.fr/Deuxfleurs/diplonat) to declaratively configure the network, 
[Bottin](https://git.deuxfleurs.fr/Deuxfleurs/bottin) to manage accounts,
and many [other software](https://git.deuxfleurs.fr).
Individually, I maintain a community repository for [Chez Scheme](https://copr.fedorainfracloud.org/coprs/superboum/chez-scheme/).
I have also some occasional contributions to some free software, like
 [Jenkins](https://github.com/jenkinsci/git-client-plugin/pull/214), [LXC](https://github.com/lxc/lxc/commit/f4152036dd29d59c99e6a9415d6ea121f69c88ec), [Matrix](https://github.com/matrix-org/synapse/pull/5138) or [Plume](https://git.joinplu.me/Plume/Plume/pulls/825).

Links: [Gitea](https://git.deuxfleurs.fr/quentin) --- [Github](https://github.com/superboum)

## Professional experience

I have a computer engineering degree from [INSA Rennes](http://www.insa-rennes.fr).
During my studies, I did internships at [OpenStudio](http://www.openstudio.fr), [Digitevent](http://www.digitevent.net), [Créancio](http://www.creancio.com), [Orckestra](https://www.orckestra.com) and the [ANSSI](https://ssi.gouv.fr).
I pursued with a Ph.D. degree in the [WIDE](https://team.inria.fr/wide/) team at [Inria Rennes](https://www.inria.fr/centre/rennes).
I worked on anonymity networks (Tor) and gossip protocols. I published the following documents:

  - Yérom-David Bromberg, Quentin Dufour, Davide Frey. Multisource Rumor Spreading with Network Coding. INFOCOM 2019 - IEEE International Conference on Computer Communications, Apr 2019, Paris, France. pp.1-10. [⟨hal-01946632⟩](https://hal.inria.fr/hal-01946632)
  - Quentin Dufour, under the supervision of Yérom-David Bromberg. High-throughput real-time onion networks to protect everyone’s privacy. [⟨theses-s204646⟩](http://theses.fr/s204646) 


Links: [Thesis Manuscript](/these.pdf) --- [CV](/cv.pdf) --- [LinkedIn](https://www.linkedin.com/in/qdufour/)


## Contact me

E-mail: `quentin``@``dufour.io`.  
Matrix: `@``quentin``:``deuxfleurs.fr`

Links: [SSH Key](/assets/misc/quentin_ssh.txt) --- [PGP Key](/assets/misc/quentin_pgp.txt)

## Friend websites

  * [Alex Auvolat](https://adnab.me/) - Computer science, distributed system
  * [Coline Aubert](https://colineaubert.com/) - Graphic design
  * [Esther Bouquet](https://estherbouquet.com) - Space and graphic design
  * [Loïck Bonniot](https://blog.lesterpig.com/) - {Dev, Hack, Science, Privacy, OSS}
  * [Tristan Claverie](https://blog.tclaverie.eu/) - Security
  * [Louison Gitzinger](https://louisongitzinger.com/) - Music & Machine Learning, Android
  * [Jean Guégant](https://jguegant.github.io) - C++, security, game dev
  * [Florian Le Minoux](https://darkgallium.github.io/) - System, Linux
  * [Adrien Luxey](https://luxeylab.net/) - Research, decentralized networks
  * [Maximilien Richer](http://mricher.fr/) - System, DevOps

{% include footer.html %}
