Marque de la laque: 3DLAC.



```gcode
G21 ;metric values
M82 ;set extruder to absolute mode
M107 ;start with the fan off
M280 P0 S160; BL-Touch Alarm realease
G4 P100; Delay for BL-Touch
G28; home
M280 P0 S160 ; BLTouch alarm release
G4 P100 ; delay for BLTouch
G29; Auto leveling
M420 Z5 ; LEVELING_FADE_HEIGHT Real activation and set parameters (if not set here, Z-Compensation failed)
M500; Write data carto G29
G92 E0 ;zero the extruded length
G1 F200 E3 ;extrude 3mm of feed stock
G92 E0 ;zero the extruded length again
G1 F4200
M117 Printing...
```
