## Boot a VM

Check launch.sh

## Create basic partitioning

```bash
sudo parted /dev/sda -- mklabel gpt

# uefi part
sudo parted /dev/sda -- mkpart ESP fat32 1MiB 512MiB
sudo parted /dev/sda -- set 1 esp on
sudo mkfs.fat -F 32 -n boot /dev/sda1

# encrypted part
sudo parted /dev/sda -- mkpart cryptroot 512MiB 100%
```

Setup the crypted boot:

```bash
sudo cryptsetup luksFormat /dev/sda2 # need to set the password
sudo cryptsetup open /dev/sda2 cryptlvm

sudo pvcreate /dev/mapper/cryptlvm
sudo vgcreate EncryptedOS /dev/mapper/cryptlvm

sudo lvcreate -L 8G EncryptedOS -n swap
sudo lvcreate -l 100%FREE EncryptedOS -n root

mkfs.ext4 -L nixos /dev/MyVolGroup/root
mkswap -L cryptswap /dev/MyVolGroup/swap
```

## Mount our stuff!

```
sudo swapon -L cryptswap
sudo mount /dev/disk/by-label/nixos /mnt
sudo mkdir /mnt/boot
sudo mount /dev/disk/by-label/boot /mnt/boot
```

## Generate the configuration

```bash
nixos-generate-config --root /mnt
```

And then we need to update `/mnt/etc/nix/configuration.nix` to add luks to the initrd:

```
{
  # snip...
  boot.initrd.luks.devices."cryptlvm".device = "/dev/disk/by-partlabel/cryptroot";
  # snip...
}
```

*Another tutorial updates the hardware-configuration.nix but it seems to be a bad practise as the file may be overwritten in the future by some Nix tools*

*Do not forget to put "s" at the end when required, do not forget the semi colon at the end of the expression*.

When ready, run:

```bash
sudo nixos-install
```

The tool assumes that you mounted your future system on `/mnt`.

Type the password you want for your root user, wait that the program returns and poweroff:

```bash
sudo poweroff
```

## Booting on a fresh NixOS

*Show how we changed the command line*

Start your VM, first type your cryptsetup password.
Then login with the root account (you just created the password with `nixos-install`).

If you made a change to your configuration.nix, you can apply it with:

```
nixos-rebuild switch
```

You can update your system by running:

```bash
nix-channel --update nixos
nixos-rebuild switch
```

## Setting up my users

I chose to go for the declarative way.

I first set:

```
users.mutableUsers = false;
```

Then I create my user:

```
users.users.quentin = {
  isNormalUser = true;
  home = "/home/quentin";
  description = "Quentin Dufour";
  extraGroups = [ "wheel" "networkmanager" ];
  hashedPassword = ""; # compute with mkpasswd -m sha-512
}
```

## Setting up my DE

I chose Sway which is an i3 clone for Wayland.

```
programs.sway.enable = true;
```

It is covered here: https://nixos.org/manual/nixos/stable/index.html#sec-wayland

I also added some programs:

```
wget
vim
nyxt
alacritty
```


## Sources 

Comment configurer une machine UEFI :
  - https://papy-tux.legtux.org/doc1310/index.html

Comment installer simplement :
  - https://nixos.org/manual/nixos/stable/#sec-installation

Comment chiffrer :
  - https://ramblings.henryjenkins.name/posts/going-from-zero-to-nixos/


