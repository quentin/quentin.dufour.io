# My Blog

```bash
nix-shell -p bundler
bundle install
bundle exec jekyll build
source ~/.awsrc
aws s3 sync _site/ s3://quentin.dufour.io/
```
