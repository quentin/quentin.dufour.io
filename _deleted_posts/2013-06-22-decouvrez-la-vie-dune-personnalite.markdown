---
layout: post
slug: decouvrez-la-vie-dune-personnalite
status: publish
title: Découvrez la vie d'une personnalité
desc: Arte propose tous les jours une petite vidéo humoristique sur une personnalité
categories:
- media
tags:
- video
- arte
---

Arte propose depuis l'année dernière une série à la publication quotidienne retraçant la vie d'une célébrité. 
Maintenant à sa deuxième saison, cette dernière est programmée en début d'été pendant environ deux mois.
Ce qui fait sa particularité est son ton totalement décalé mais surtout sa réalisation. Uniquement avec des petits objets qui représentent à merveille les personnages et le décor.

Mais ne sous-estimons pas le côté instructif de l'émission, celle-ci à quand même pour objectif de résumer en 2 minutes la vie d'une célébrité !
Certains diront que les petits objets s'attardent un peu trop sur les détails et anecdotes des grandes célébrités, voir même l'accuse de déformation.
Mais après tout, je vous parle d'une série qui s'appelle "Tout est vrai ou presque".

Vous êtes maintenant prévenu !

###On regarde ça tout de suite !

<iframe width="560" height="315" src="http://www.youtube.com/embed/I3Zioc63AuA?list=PLE6306B8827EFCCEF" frameborder="0" allowfullscreen></iframe>


