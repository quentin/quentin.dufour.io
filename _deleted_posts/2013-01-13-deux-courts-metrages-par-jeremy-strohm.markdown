---
layout: post
slug: deux-courts-metrages-par-jeremy-strohm
status: publish
title: Deux courts métrages par Jérémy Strohm
desc: Quelque fois on fait d'heureuses découvertes sur Youtube, en voici une.
categories:
- media
tags:
- youtube
- courts métrages
---

Je m'intéresse particulièrement aux courts métrages. En surfant sur Youtube, j'ai eu la chance de découvrir quelques réalisations de Jérémy Strohm qui selon moi mériterait d'être plus connu.
Tout d'abord, un drôle de court métrage qui prend place dans un Paris des années 1995, je ne vous en dis pas plus, je vous laisse découvrir...

<iframe width="853" height="480" src="https://www.youtube.com/embed/6EaDP_2YJko?rel=0" frameborder="0" allowfullscreen></iframe>

Ensuite, je vous propose une réalisation plus récente, ma préférée : Rien à dire.

<iframe width="853" height="480" src="https://www.youtube-nocookie.com/embed/jT5zYvuHE7s?rel=0" frameborder="0" allowfullscreen></iframe>

A chaque fois je suis sidéré par les chutes qui sont excellentes. Les plans sont magnifiques, les sujets abordés surprenants.
En tout cas, si vous aussi vous avez aimé, vous pouvez jeter un oeil à sa [chaine youtube]
(https://www.youtube.com/user/jeremystrohm/).
