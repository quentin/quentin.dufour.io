---
title: Les objets en Processing
permalink: ex/processing-objets/
profile: false
---

# Les objets en Processing

<br/>

## Pré-Requis

Pour commencer, je recommande la lecture de l'article [Objects](https://processing.org/tutorials/objects/) par Daniel Shiffman sur le site web de Processing.

## Présentation de l'exercice

Une fois l'article lu, je vous propose l'exercice suivant : la création d'un outil de gestion de série.
Notre objectif est dans un premier temps d'afficher l'ensemble des épisodes d'une série, avec ses saisons.
Dans un second temps, on va vouloir calculer le temps que ça va nous prendre de visionner la série.

On va décomposer la chose en 3 objets : Épisode, Saison et Série.
Une série contient des saisons, chaque saison contient des épisodes.

## L'objet Épisode

Je vous conseille de commencer par implémenter l'objet Épisode. Voilà un exemple pour tester votre implémentation :

```java
void setup() {
  Episode e = new Episode("Une étude en rose",88);
  println(e); // équivalent à println(e.toString());
  println(e.getDuration());
}
```

Ce qui doit afficher dans la console :

```raw
Une étude en rose (88 min)
88
```

## L'objet Saison

Maintenant que votre objet épisode fonctionne comme vous voulez, attaquons nous à l'objet Saison :

```java
void setup() {
  Saison s;
  s = new Saison(1);
  s.ajouterEpisode(new Episode("Une étude en rose",88));
  s.ajouterEpisode(new Episode("Le banquier aveugle", 88));
  s.ajouterEpisode(new Episode("Le grand jeu", 89));

  println(s); // équivalent à println(s.toString())
  println(s.getDuration());
}
```

Ce qui devrait afficher :

```raw
--- Saison 1 ---
Une étude en rose (88 min)
Le banquier aveugle (88 min)
Le grand jeu (89 min)

265
```

## L'objet Série

Voilà comment nous voulons utiliser l'objet Série :

```java
  Serie sherlock = new Serie("Sherlock");
  Saison s;
  
  s = new Saison(1);
  s.ajouterEpisode(new Episode("Une étude en rose",88));
  s.ajouterEpisode(new Episode("Le banquier aveugle", 88));
  s.ajouterEpisode(new Episode("Le grand jeu", 89));
  sherlock.ajouterSaison(s);

  s = new Saison(2);
  s.ajouterEpisode(new Episode("Un scandale à Buckingham", 89));
  s.ajouterEpisode(new Episode("Les chiens de Baskerville", 88));
  s.ajouterEpisode(new Episode("La chute du Reichenbach", 88));
  sherlock.ajouterSaison(s);

  s = new Saison(3);
  s.ajouterEpisode(new Episode("Le cercueil vide", 86));
  s.ajouterEpisode(new Episode("Le signe des trois", 86));
  s.ajouterEpisode(new Episode("Son dernier coup d'éclat", 89));
  sherlock.ajouterSaison(s);

  s = new Saison(4);
  s.ajouterEpisode(new Episode("Les six Thatchers", 88));
  s.ajouterEpisode(new Episode("Le détective affabulant", 89));
  s.ajouterEpisode(new Episode("Le dernier problème", 89));
  sherlock.ajouterSaison(s);

  println(sherlock); // équivalent à println(sherlock.toString())
  println(sherlock.getDuration());
```

Ce qui devrait afficher :

```raw
=== Série Sherlock ===
--- Saison 1 ---
Une étude en rose (88 min)
Le banquier aveugle (88 min)
Le grand jeu (89 min)

--- Saison 2 ---
Un scandale à Buckingham (89 min)
Les chiens de Baskerville (88 min)
La chute du Reichenbach (88 min)

--- Saison 3 ---
Le cercueil vide (86 min)
Le signe des trois (86 min)
Son dernier coup d'éclat (89 min)

--- Saison 4 ---
Les six Thatchers (88 min)
Le détective affabulant (89 min)
Le dernier problème (89 min)


1057
```

## Pour aller plus loin

Vous pouvez ajouter des fonctionnalités supplémentaires à l'objet Série, comme la possibilité d'avoir la durée en heure / minutes plutôt qu'en minute seulement, ou encore de renvoyer la saison la plus longue ou la plus courte, le nombre d'épisode ou le nombre d'épisodes moyen par saisons, etc.
