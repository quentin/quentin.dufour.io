---
date: '2012-07-26 00:00:00'
layout: post
slug: le-meme-en-mieux-
status: publish
title: Le même en mieux ! 
desc: Just because I broke up everything 
categories:
- web
tags:
- hello world
- jekyll
---

Suite au crash du disque dur de mon serveur, qui venait de fêter ses 12 ans en grande pompe, j'ai commencé à repenser tout "l'écosystème" qui tourne dessus. Ruby On Rails semblait bien inutile pour faire tourner mon blog. En plus de tous les inconvénients que ça amenait en l'intégrant à Apache, ça ralentissait le système... Je suis donc passé à Jekyll !
###Jekyll, Tukyll, Ellekyll...
Jekyll permet de générer un site web statique (composé uniquement de fichiers HTML). Ca simplifie la vie, une fois votre site généré, il ne vous reste plus qu'à l'héberger, et un hébergement comme ça, c'est facile à trouver gratuitement, et ça consomme rien. Votre serveur web n'a plus qu'à envoyer les pages sans traitement.
###L'âge de pierre
Pas tout à fait, parce que le squelette qui reste sur votre ordinateur et que vous générez a bien la forme d'un site web en PHP avec le design de base dans lequel on intègre nos élèments. C'est super souple, et du coup faire son propre design est super facile (ok, pomper celui des autres l'est encore plus...)
