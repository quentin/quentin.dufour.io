---
date: '2012-10-04 20:03:00'
layout: post
slug: mfiez-vous-de-larp-spoof
status: publish
title: Méfiez vous de l'arp spoof
desc: L'ARP Spoof est une attaque sournoise et puissante sur un réseau local, découvrez pourquoi !
categories:
- reseau
tags:
- arp spoof
- attaque
- hack
---

L'arp spoof est une attaque permettant à une personne présente sur le même réseau local (LAN) que vous,
de faire transiter tout le trafic par son ordinateur. Concrètement, la personne qui la mettra en place pourra voir et même modifier tout ce que vous faites !

Il n'y a pas de protection magique, elle se base sur le fonctionnement d'un réseau. Vous pouvez toujours définir manuellement l'adresse MAC de la passerelle grâce à la commande arp sous Linux. En effet, via cette protection, notre pirate pourra toujours tenter de se faire passer pour la passerelle, votre ordinateur n'écoutera que lui.

En conclusion, quand vous êtes sur un réseau public, ne faites pas confiance aux sites webs, il est toujours possible, d'une manière ou d'une autre, de voir et modifier ce que vous faites. N'en devenez pas parano pour autant, et bon surf !
