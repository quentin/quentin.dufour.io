---
layout: post
slug: partageons-la-musique-professor-kliq
status: publish
title: Partageons la musique Professor Kliq
desc: Musique libre gratuite et cool
categories:
- media
tags:
- jamendo
- musique
---

Professor Kliq a réalisé plusieurs albums electro-techno ces dernières années.
Cependant vous ne trouverez pas son dernier album sur Amazon ou Itunes, mais plutot du côté de Jamendo. En effet, notre musicien publie ses musiques sous license Creative Common. Vous pouvez donc les télécharger gratuitement et légalement !
Et ce n'est pas un petit rigolo amateur comme ça, certaine de ses musiques ont été utilisées dans diverses publicités. Par exemple Nadeo s'en est servi pour la promotion de Trackmania 2

###Pour en savoir plus :
http://www.jamendo.com/fr/artist/339989/professor-kliq
http://www.professorkliq.com
